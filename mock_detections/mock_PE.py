import vt_utils as vt
from numpy import *
from astropy import units as u
from scipy.stats import truncnorm
from scipy.stats import gaussian_kde
from scipy.interpolate import interp1d

def ptheta_func():
    thetas = vt.draw_thetas(10000)
    thetas = concatenate((thetas, [0.0, 1.0]))
    thetas = sort(thetas)
    theta_icdf = interp1d(linspace(0, 1, thetas.shape[0]), thetas)
    theta_kde = gaussian_kde(thetas)
    ts = linspace(0,1,1024)
    ps = theta_kde(ts) + theta_kde(-ts) + theta_kde(2-ts)
    ptheta = interp1d(ts,ps,bounds_error=False,fill_value = 0.0)
    return ptheta

#uncert_default = {'threshold_snr': 8, 'mc': 0.07, 'Theta': 0.15*1.4, 'eta': 0.015} #try to increase mass uncertainties
uncert_default = {'threshold_snr': 8, 'mc': 0.08, 'Theta': 0.15*1.4, 'eta': 0.022}

def mchirp(m1,m2):
    return (m1*m2)**0.6/(m1+m2)**0.2

def etafunc(m1,m2):
    return (m1*m2)/(m1+m2)**2

def m1m2_from_mceta(mc,eta):
    mtot = mc/eta**0.6
    m1m2 = eta*mtot**2
    m2 = (mtot-sqrt(mtot**2-4*m1m2))/2.0
    m1 = (mtot+sqrt(mtot**2-4*m1m2))/2.0
    return m1, m2

def dm1m2_dMceta(m1,m2): #Jacobian transformation
    eta = etafunc(m1,m2)
    return (m1+m2)**2/((m1-m2)*eta**0.6)

def ddL_drho(osnr_gpc_func,m1det,m2det,rho,theta):
    return osnr_gpc_func(m1det,m2det,grid=False)/rho**2*theta

def generate_detected_from_true_list(m1, m2, z, osnr_interp, cosmo_dict, osnr_interp_dist = u.Gpc, t = None, uncert=uncert_default):
    if t is None:
        t = vt.draw_thetas(len(m1))
    m1det = m1*(1+z)
    m2det = m2*(1+z)
    dl = cosmo_dict['dL'](z)*((cosmo_dict['dist_Mpc']*u.Mpc).to(osnr_interp_dist).value)
    rho_true = t*osnr_interp(m1det, m2det, grid=False)/dl
    rho_obs = rho_true+random.randn(len(m1))
    det_sel = rho_obs > uncert['threshold_snr']
    return {'m1': m1[det_sel], 'm2': m2[det_sel], 'z': z[det_sel], 'theta': t[det_sel], 'lum_dist': dl[det_sel], 'z': z[det_sel], 'rho_obs': rho_obs[det_sel], 'detected_index': array(arange(len(m1)))[det_sel]}

def generate_obs_from_true_list(m1t, m2t, zt, osnr_interp, cosmo_dict, osnr_interp_dist = u.Gpc, PEsamps = None, t = None, uncert=uncert_default):
    detected_dict = generate_detected_from_true_list(m1t, m2t, zt, osnr_interp, cosmo_dict, osnr_interp_dist, t)
    m1, m2, z = detected_dict['m1'], detected_dict['m2'], detected_dict['z']
    mc = mchirp(m1,m2)*(1+z)
    eta = etafunc(m1, m2)
    rho_obs = detected_dict['rho_obs']
    smc = uncert['threshold_snr']/rho_obs*uncert['mc']
    mcobs = random.lognormal(mean=log(mc), sigma=smc)
    seta = uncert['threshold_snr']/rho_obs*uncert['eta']
    etaobs = eta+seta*truncnorm.rvs((0.0-eta)/seta,(0.25-eta)/seta,size=len(m1))
    st = uncert['threshold_snr']/rho_obs*uncert['Theta']
    theta = detected_dict['theta']
    tobs = theta+st*truncnorm.rvs((0.0-theta)/st, (1.0-theta)/st, size=len(m1))
    if PEsamps is None:
        m1det_obs, m2det_obs = m1m2_from_mceta(mcobs,etaobs)
        dl_obs = tobs*osnr_interp(m1det_obs, m2det_obs, grid=False)/rho_obs
        dl_obs = (dl_obs*osnr_interp_dist).to(u.Mpc).value/cosmo_dict['dist_Mpc']
        z_obs = cosmo_dict['z_at_dL'](dl_obs)
        m1_obs = m1det_obs/(1+z_obs)
        m2_obs = m2det_obs/(1+z_obs)
        return {'m1_obs': m1_obs, 'm2_obs': m2_obs, 'z_obs': z_obs,'t_obs': tobs, 'detected_dict': detected_dict}
    else:
        ptheta = ptheta_func()
        m1_samps_list = []
        m2_samps_list = []
        z_samps_list = []
        wts_list = []
        for i in range(len(m1)):
            mc_samps = random.lognormal(mean=log(mcobs[i]),sigma=smc[i],size=PEsamps)
            eta_mean = eta[i]+seta[i]*random.randn()
            eta_samps = eta_mean+seta[i]*truncnorm.rvs((0.0-eta_mean)/seta[i],(0.25-eta_mean)/seta[i],size=PEsamps)
            t_mean = theta[i]+st[i]*random.randn()
            theta_samps = t_mean+st[i]*truncnorm.rvs((0.0-t_mean)/st[i], (1.0-t_mean)/st[i], size=PEsamps)
            rho_samps = rho_obs[i]+random.randn(PEsamps)
            m1det_samps, m2det_samps = m1m2_from_mceta(mc_samps,eta_samps)
            dl_samps = theta_samps*osnr_interp(m1det_samps,m2det_samps,grid=False)/rho_samps
            dl_samps = (dl_samps*osnr_interp_dist).to(u.Mpc).value/cosmo_dict['dist_Mpc']
            z_samps = cosmo_dict['z_at_dL'](dl_samps)
            wts = ddL_drho(osnr_interp, m1det_samps, m2det_samps, rho_samps, theta_samps)*dm1m2_dMceta(m1det_samps,m2det_samps)*ptheta(theta_samps)*dl_samps**2
            wts /= max(wts)
            m1_samps_list.append(m1det_samps/(1.0+z_samps))
            m2_samps_list.append(m2det_samps/(1.0+z_samps))
            z_samps_list.append(z_samps)
            wts_list.append(wts)
        return {'m1_samps': m1_samps_list, 'm2_samps': m2_samps_list, 'z_samps': z_samps_list, 'weights': wts_list, 'detected_dict': detected_dict}

