# Make sure to run in pycbc environment, e.g.
# source /cvmfs/oasis.opensciencegrid.org/ligo/sw/pycbc/x86_64_rhel_7/virtualenv/pycbc-v1.14.1/bin/activate

import matplotlib as mpl
mpl.use('Agg')
import numpy as np
import sys
sys.path.append("./../code/")
from gwBackground import *
from computeTotalTime import *
sys.path.append("/home/thomas.callister/Stochastic/DetectorScramble/GW-Geodesy/src/")
from baseline import *
from makeLigoData import *
from pycbc.psd.analytical import aLIGODesignSensitivityP1200087
import matplotlib.pyplot as plt
plt.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')

year = 365.25*24*3600.
H0 = 2.2685e-18

# Get HL overlap reduction function
freqs = np.arange(10.,400.,0.25)
orf = Baseline.LigoBaseline().gamma_isotropic(freqs)

# Define true background
R0 = 30. # per Gpc^3 per yr
gamma = 1.2
mMax = 45.
alpha = 3.
beta = 3.
zpeak = 2.
OmegaGW = OmegaGW_massDistr(R0/1.e9/year,alpha,beta,zpeak,gamma,mMax,freqs,mMin=5.)
C_avg = orf*OmegaGW

# Get observation time (convert to seconds)
T = obsTime(500,R0,gamma,mMax,alpha,beta,zpeak)*year
print(T/year)

#Nevents = int(500.*1.2*year/T)
#print(Nevents)
#print(obsTime(Nevents,R0,gamma,mMax,alpha,beta,zpeak))


# Get PSD data and interpolate
psd_series = aLIGODesignSensitivityP1200087(1100,1,0)
psd_vals = psd_series.data
freq_vals = psd_series.get_sample_frequencies().data
PSDs = np.interp(freqs,freq_vals,psd_vals)

#np.savetxt('psd.txt',np.transpose([freqs,PSDs]))

# Generate noise
norm = (10.*np.pi**2.)/(3.*H0**2.)
df = freqs[1]-freqs[0]
sigma2s = np.power(norm,2.)*(1./(2.*T*df))*np.power(freqs,6.)*np.power(PSDs,2.)

# Randomly choose noise from normal distributions
noise = np.array([
np.random.normal(loc=0,scale=np.sqrt(sigma2s[i])) for i in range(len(freqs))
])

# Add to expectation value and save
C_measured = C_avg + noise

np.savetxt('./design_alpha3.0_beta3.0_zpeak2.0/stochastic_obs.dat',np.transpose([freqs,orf,OmegaGW,C_avg,C_measured,sigma2s]))

########

model = orf*np.power(freqs/25.,2./3.)
numerator = np.sum(C_measured*model/sigma2s)/np.sum(model/sigma2s)
denominator = np.sum(model**2./sigma2s)/np.power(np.sum(model/sigma2s),2.)
SNR2 = np.power(numerator,2.)/denominator
print(np.sqrt(SNR2))

print(np.sqrt(np.sum(C_avg*C_measured/sigma2s)))
print("optimal --")
print(np.sqrt(np.sum(C_avg**2./sigma2s)))

##########

fig,ax = plt.subplots()
ax.fill_between(freqs,np.sqrt(sigma2s),-np.sqrt(sigma2s),facecolor='grey',alpha=0.2,zorder=-1)
ax.plot(freqs,np.sqrt(sigma2s),color='grey',lw=0.25)
ax.plot(freqs,-np.sqrt(sigma2s),color='grey',lw=0.25)
ax.plot(freqs,C_measured,lw=0.9,color='#377eb8',alpha=0.6)
ax.plot(freqs,C_avg,lw=1.5,color='#08519c')
ax.set_xlabel('Frequency (Hz)')
ax.set_ylabel(r'$\hat C(f)$')
ax.set_xlim(10,120)
ax.set_ylim(-2e-8,2e-8)
plt.tight_layout()
plt.savefig('./design_alpha3.0_beta3.0_zpeak2.0/stochastic_observation.pdf',bbox_inches='tight')
