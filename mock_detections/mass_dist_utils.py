#define all the mass distributions, generating random masses from them, etc.
from numpy import *

def inv_cumulative_power_law(u, mmin, mmax, alpha):
    if alpha != -1:
        return (u*(mmax**(alpha+1) - mmin**(alpha+1)) + mmin**(alpha+1))**(1.0/(alpha+1))
    elif alpha == -1:
        return exp(u*(log(mmax)-log(mmin))+log(mmin))

def generate_powerlaw_masses(mmin, mmax, alpha, beta, nsamps):
    """
    note convention that alpha is the power-law slope, not -alpha!
    """
    m1s = inv_cumulative_power_law(random.rand(nsamps),mmin,mmax,alpha)
    m2s = inv_cumulative_power_law(random.rand(nsamps),mmin,m1s,beta)
    return m1s, m2s

def modelB(m1,m2,alpha,beta,mmin,mmax):
    out = zeros_like(m1)
    sel = (m1>=m2) & (m1<=mmax) & (m2>=mmin)
    out[sel] = (alpha+1)*m1[sel]**alpha/(mmax**(alpha+1)-mmin**(alpha+1))*(beta+1)*m2[sel]**beta/(m1[sel]**(beta+1)-mmin**(beta+1))
    return out
