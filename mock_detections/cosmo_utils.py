import astropy.cosmology as cosmo
import astropy.units as u
import astropy.constants as const
import scipy.interpolate as sip
from scipy.integrate import cumtrapz
from pylab import *

def interp_cosmology(zmin=0.0,zmax=5.0,zres = 0.01, cosmology=cosmo.Planck15, dist_unit = u.Mpc):
    '''return interpolated function for dL(z), z(dL), dC(z), z(dC), 1/E(z), dVc/dz(z)
    cosmology can be one of the named cosmologies, or cosmo.FlatLambdaCDM(H0 = 70, Om0 = 0.3)
    '''
    nz = int(round((zmax-zmin)/zres)) 
    zs = linspace(zmin,zmax,nz)
    dLs = cosmology.luminosity_distance(zs).to(dist_unit).value
    dCs = cosmology.comoving_distance(zs).to(dist_unit).value
    inve = sip.interp1d(zs,cosmology.inv_efunc(zs))
    lumdist = sip.interp1d(zs,dLs)
    z_at_dL = sip.interp1d(dLs,zs)
    comoving_dist = sip.interp1d(zs,dCs)
    inv_comovingdist = sip.interp1d(dCs,zs)
    diff_comoving_vol = sip.interp1d(zs,4.0*pi*cosmology.differential_comoving_volume(zs).to(dist_unit**3/u.sr).value)
    cosmo_dict = {'H0': cosmology.H0.value, 'dist_Mpc': (1.0*dist_unit).to(u.Mpc).value, 'dL': lumdist, 'z_at_dL': z_at_dL, 'dC': comoving_dist, 'z_at_dC': inv_comovingdist, 'inv_efunc': inve, 'diff_comoving_vol': diff_comoving_vol, 'dH': cosmology.hubble_distance.to(dist_unit).value}
    return cosmo_dict

def lalprior_func(cosmo_dict,dist_unit=u.Mpc):
    '''returns an interpolated function of z that gives the lalinference prior in terms of source-frame masses and redshift p_LI(m1,m2,z)
    '''
    dist_Mpc_old = cosmo_dict['dist_Mpc']
    dist_Mpc_new = (1.0*dist_unit).to(u.Mpc).value
    dist_fact = dist_Mpc_new/dist_Mpc_old
    dH = cosmo_dict['dH']
    dL_func = cosmo_dict['dL']
    comoving_dist_func = cosmo_dict['dC']
    inv_efunc = cosmo_dict['inv_efunc']
    func = lambda z: (dL_func(z)*dist_fact)**2*(1+z)**2*(comoving_dist_func(z)*dist_fact+(1+z)*dH*dist_fact*inv_efunc(z))
    return func

def ppop_dL_func(cosmo_dict,H0_new = None,dist_unit = u.Mpc, g = 0.0): #g parametrizes redshift distribution
    if H0_new:
        H0_fact = H0_new/cosmo_dict['H0']
    else:
        H0_fact = 1.0
    dist_Mpc_old = cosmo_dict['dist_Mpc']
    dist_Mpc_new = (1.0*dist_unit).to(u.Mpc).value
    dist_fact = dist_Mpc_new/dist_Mpc_old
    z_at_dL = cosmo_dict['z_at_dL']
    dH = cosmo_dict['dH']
    comoving_dist = cosmo_dict['dC']
    inv_efunc = cosmo_dict['inv_efunc']
    diff_comoving_vol = cosmo_dict['diff_comoving_vol']
    def ppop_dL(lumdist):
        z = z_at_dL(lumdist/dist_fact*H0_fact)
        ddLdz = H0_fact*(comoving_dist(z)+(1+z)*dH*inv_efunc(z))
        return H0_fact**3*diff_comoving_vol(z)*(1+z)**(g-1.0)/ddLdz
    return ppop_dL

def ppop_z_func(cosmo_dict,g = 0.0):
    diff_comoving_vol = cosmo_dict['diff_comoving_vol']
    def ppop_z(z):
        return diff_comoving_vol(z)*(1.0+z)**(g-1.0)
    return ppop_z

def ppop_z_fit_func(cosmo_dict, zmin = 0.001, zmax = 2.0, zres = 0.01,  g = 0.0, return_coeffs = True):
    ''' return a rational fitting function to the redshift distribution
    and optionally the coefficients to replicate this function
    '''
    nz = int(round((zmax-zmin)/zres))
    zs = linspace(zmin,zmax,nz)[1:]
    diff_comoving_vol = cosmo_dict['diff_comoving_vol']
    b = zs**2/diff_comoving_vol(zs)
    a = column_stack((ones_like(zs), zs, zs*zs, zs*zs*zs, zs*zs*zs*zs))
    x = linalg.lstsq(a,b)[0]
    def ppop_z_fit(z):
        return z**2/(x[0]+x[1]*z+x[2]*z**2+x[3]*z**3+x[4]*z**4)*(1.0+z)**(g-1.0)
    if return_coeffs:
        return ppop_z_fit, x 
    else:
        return ppop_z_fit
    

def draw_volume_dLs(cosmo_dict,dlmax,n=4096,H0_new=None, dist_unit = u.Mpc):
    if H0_new:
        H0_fact = H0_new/cosmo_dict['H0']
    else:
        H0_fact = 1.0
    dist_Mpc_old = cosmo_dict['dist_Mpc']
    dist_Mpc_new = (1.0*dist_unit).to(u.Mpc).value
    dist_fact = dist_Mpc_new/dist_Mpc_old
    z_at_dL = cosmo_dict['z_at_dL']
    z_at_dC = cosmo_dict['z_at_dC']
    dcmax = dlmax/(1+z_at_dL(dlmax*H0_fact))
    com_dists = (random.rand(n)*dcmax**3)**(1./3.)
    return com_dists*(1+z_at_dC(com_dists*H0_fact))*dist_fact

def draw_zs_func(cosmo_dict,zmax,zres = 0.01, g = 0.0):
    ppop_z = ppop_z_func(cosmo_dict,g)
    nz = int(round((zmax-0.0)/zres)) 
    zs = linspace(0.0,zmax,nz)
    zcumdist = cumtrapz(ppop_z(zs), zs, initial=0)
    zcumdist /= zcumdist[-1]
    zinv_cum = sip.interp1d(zcumdist,zs)
    return zinv_cum #call this function zinv_cum(random.rand(nsamps))

