import numpy as np

def estimateBounds(m1_med,dm1_low,dm1_high,m2_med,dm2_low,dm2_high,Mc_med,dMc_low,dMc_high,z_med,dz_low,dz_high):

    Mc_med_d = Mc_med*(1.+z_med) 
    Mc_low_d = (Mc_med-dMc_low)*(1.+z_med-dz_low)
    Mc_high_d = (Mc_med+dMc_high)*(1.+z_med+dz_high)
    Mc_std_d = (Mc_high_d-Mc_low_d)/2.

    q_med = m2_med/m1_med
    q_low = (m2_med-dm2_low)/(m1_med+dm1_high)
    q_high = (m2_med+dm2_high)/(m1_med-dm1_low)
    q_std = (q_high-q_low)/2.

    z_std = (dz_high+dz_low)/2.

    return Mc_med_d,Mc_std_d,q_med,q_std,z_med,z_std
