import matplotlib.pyplot as plt
import numpy as np

data = np.load('samples_interpFraction.npy')

fig,ax = plt.subplots(figsize=(4,3))
ax.hexbin(data[:,0],data[:,1],cmap='Blues',gridsize=20)
ax.set_xlabel(r'$\alpha$',fontsize=10)
ax.set_ylabel(r'$\lambda$',fontsize=10)
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_xlim([-3,4])
ax.set_ylim(-40,5)
plt.tight_layout()
ax.tick_params(labelsize=8)
plt.savefig('./lambda_alpha.pdf',bbox_inches='tight')

fig,ax = plt.subplots()
ax.hist(data[:,0],bins=15)
plt.savefig('alpha.pdf')

fig,ax = plt.subplots()
ax.hist(data[:,1],bins=15)
plt.savefig('lambda.pdf')

fig,ax = plt.subplots()
ax.hist(data[:,2],bins=15)
plt.savefig('mMax.pdf')
