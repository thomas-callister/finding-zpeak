import numpy as np
import emcee as mc
import acor
from makePosteriors import makePosterior
from estimateBounds import estimateBounds
import matplotlib.pyplot as plt
import sys
from scipy.interpolate import LinearNDInterpolator

# Prior bounds
a_Min = -4.
a_Max = 5.
lmbda_Min = -50.
lmbda_Max = 30.
mMax_Min = 31.
mMax_Max = 100.
mMin = 5.
#mMax = 39.

# BBH data
gw150914_Mc_mean,gw150914_Mc_std,gw150914_q_mean,gw150914_q_std,gw150914_z_mean,gw150914_z_std\
    = estimateBounds(35.6,3.0,4.8,30.6,4.4,3.0,28.6,1.5,1.6,0.09,0.03,0.03)
gw151012_Mc_mean,gw151012_Mc_std,gw151012_q_mean,gw151012_q_std,gw151012_z_mean,gw151012_z_std\
    = estimateBounds(23.3,5.5,14.0,13.6,4.8,4.1,15.2,1.1,2.0,0.21,0.09,0.09)
gw151226_Mc_mean,gw151226_Mc_std,gw151226_q_mean,gw151226_q_std,gw151226_z_mean,gw151226_z_std\
    = estimateBounds(13.7,3.2,8.8,7.7,2.6,2.2,8.9,0.3,0.3,0.09,0.04,0.04)
gw170104_Mc_mean,gw170104_Mc_std,gw170104_q_mean,gw170104_q_std,gw170104_z_mean,gw170104_z_std\
    = estimateBounds(31.0,5.6,7.2,20.1,4.5,4.9,21.5,1.7,2.1,0.19,0.08,0.07)
gw170814_Mc_mean,gw170814_Mc_std,gw170814_q_mean,gw170814_q_std,gw170814_z_mean,gw170814_z_std\
    = estimateBounds(30.7,3.0,5.7,25.3,4.1,2.9,24.2,1.1,1.4,0.12,0.04,0.03)
gw170608_Mc_mean,gw170608_Mc_std,gw170608_q_mean,gw170608_q_std,gw170608_z_mean,gw170608_z_std\
    = estimateBounds(10.9,1.7,5.3,7.6,2.1,1.3,7.9,0.2,0.2,0.07,0.02,0.02)

# Generate samples
m1_samples_150914,m2_samples_150914,z_samples_150914 = makePosterior(\
    gw150914_Mc_mean,gw150914_Mc_std,\
    gw150914_q_mean,gw150914_q_std,\
    gw150914_z_mean,gw150914_z_std,\
    1000)
m1_samples_151012,m2_samples_151012,z_samples_151012 = makePosterior(\
    gw151012_Mc_mean,gw151012_Mc_std,\
    gw151012_q_mean,gw151012_q_std,\
    gw151012_z_mean,gw151012_z_std,\
    1000)
m1_samples_151226,m2_samples_151226,z_samples_151226 = makePosterior(\
    gw151226_Mc_mean,gw151226_Mc_std,\
    gw151226_q_mean,gw151226_q_std,\
    gw151226_z_mean,gw151226_z_std,\
    1000)
m1_samples_170104,m2_samples_170104,z_samples_170104 = makePosterior(\
    gw170104_Mc_mean,gw170104_Mc_std,\
    gw170104_q_mean,gw170104_q_std,\
    gw170104_z_mean,gw170104_z_std,\
    1000)
m1_samples_170814,m2_samples_170814,z_samples_170814 = makePosterior(\
    gw170814_Mc_mean,gw170814_Mc_std,\
    gw170814_q_mean,gw170814_q_std,\
    gw170814_z_mean,gw170814_z_std,\
    1000)
m1_samples_170608,m2_samples_170608,z_samples_170608 = makePosterior(\
    gw170608_Mc_mean,gw170608_Mc_std,\
    gw170608_q_mean,gw170608_q_std,\
    gw170608_z_mean,gw170608_z_std,\
    1000)

sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104]),\
    4:np.array([m1_samples_170814,z_samples_170814]),\
    5:np.array([m1_samples_170608,z_samples_170608])\
    }

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('redshiftData.dat',usecols=(0,1,2,3),unpack=True)

# Load detection fraction data, set up interpolator
resultsDict = np.load('detectionFractionData.npy')[()]
coords = resultsDict['coords']
logfracs = resultsDict['logfracs']
logDetectionFraction = LinearNDInterpolator(coords,logfracs,rescale=True)

def loglikelihoodIndividual(a,lmbda,m1_samples,z_samples):

    # Construct redshift probability distribution
    ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,lmbda-1.)
    ref_pzs = ref_pzs/np.sum(ref_pzs)

    if a==1:
        p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
    else:
        p_m1 = (1.-a)*np.power(m1_samples,-a)/(np.power(mMax,1.-a)-np.power(mMin,1.-a))
    p_z = np.interp(z_samples,ref_zs,ref_pzs)

    # Zero out probabilities above mMax
    p_m1[m1_samples>mMax] = 0

    # Evaluate marginalized likelihood
    nSamples = m1_samples.size
    pEvidence = np.sum(p_m1*p_z)/nSamples

    return np.log(pEvidence)

def loglikelihood(c):

    # Read parameters
    a = c[0]
    lmbda = c[1]
    mMax = c[2]

    if a<a_Min or a>a_Max or lmbda<lmbda_Min or lmbda>lmbda_Max or mMax<mMax_Min or mMax>mMax_Max:
        return -np.inf

    else:

        # Construct redshift probability distribution
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,lmbda-1.)
        ref_pzs = ref_pzs/np.sum(ref_pzs)

        logP = 0.
        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            
            # Mass probability
            if a==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-a)*np.power(m1_samples,-a)/(np.power(mMax,1.-a)-np.power(mMin,1.-a))

            # Zero out probabilities above mMax
            p_m1[m1_samples>mMax] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_z)/nSamples

            # Add
            logP += np.log(pEvidence)

        print(a,lmbda,mMax,logP)

        return logP - 0.*logDetectionFraction(a,lmbda,mMax)

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 20
    initial_as = np.random.random(nWalkers)*(a_Max-a_Min)+a_Min
    initial_lmbdas = np.random.random(nWalkers)*(lmbda_Max-lmbda_Min)+lmbda_Min
    initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    initial_walkers = np.transpose([initial_as,initial_lmbdas,initial_mMaxs])
    #initial_walkers = np.transpose([initial_as,initial_lmbdas])

    # Run
    nSteps = 3000
    sampler = mc.EnsembleSampler(nWalkers,3,loglikelihood)
    sampler.run_mcmc(initial_walkers,nSteps)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(3):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*3)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples_interpFraction.npy',chainDownsampled)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

