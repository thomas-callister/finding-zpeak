import numpy as np
from detectionFraction import detectionFractionFull
from makePosteriors import makePosterior
import matplotlib.pyplot as plt
from scipy.interpolate import InterpolatedUnivariateSpline

mMin = 5.
mMax = 39.

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('redshiftData.dat',usecols=(0,1,2,3),unpack=True)

def estimateBounds(m1_med,dm1_low,dm1_high,m2_med,dm2_low,dm2_high,Mc_med,dMc_low,dMc_high,z_med,dz_low,dz_high):

    Mc_med_d = Mc_med*(1.+z_med) 
    Mc_low_d = (Mc_med-dMc_low)*(1.+z_med-dz_low)
    Mc_high_d = (Mc_med+dMc_high)*(1.+z_med+dz_high)
    Mc_std_d = (Mc_high_d-Mc_low_d)/2.

    q_med = m2_med/m1_med
    q_low = (m2_med-dm2_low)/(m1_med+dm1_high)
    q_high = (m2_med+dm2_high)/(m1_med-dm1_low)
    q_std = (q_high-q_low)/2.

    z_std = (dz_high+dz_low)/2.

    return Mc_med_d,Mc_std_d,q_med,q_std,z_med,z_std

def hyperPosterior(a,lmbda,m1_samples,z_samples):

    # Construct redshift probability distribution
    ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,lmbda-1.)
    ref_pzs = ref_pzs/np.sum(ref_pzs)
    pz_interp = InterpolatedUnivariateSpline(ref_zs,ref_pzs)

    if a==1:
        p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
    else:
        p_m1 = (1.-a)*np.power(m1_samples,-a)/(np.power(mMax,1.-a)-np.power(mMin,1.-a))
    #p_z = np.interp(z_samples,ref_zs,ref_pzs)
    p_z = pz_interp(z_samples)

    # Zero out probabilities above mMax
    p_m1[m1_samples>mMax] = 0

    # Evaluate marginalized likelihood
    nSamples = m1_samples.size
    pEvidence = np.sum(p_m1*p_z)/nSamples
    #if pEvidence<0:
    #    print(p_z)
    #    print(p_m1)


    # Detection fraction
    #dFraction = detectionFractionFull(mMin,mMax,a,lmbda)
    #return pEvidence/dFraction
    return np.log(pEvidence)

if __name__=="__main__":

    gw150914_Mc_mean,gw150914_Mc_std,gw150914_q_mean,gw150914_q_std,gw150914_z_mean,gw150914_z_std\
        = estimateBounds(35.6,3.0,4.8,30.6,4.4,3.0,28.6,1.5,1.6,0.09,0.03,0.03)
    gw151012_Mc_mean,gw151012_Mc_std,gw151012_q_mean,gw151012_q_std,gw151012_z_mean,gw151012_z_std\
        = estimateBounds(23.3,5.5,14.0,13.6,4.8,4.1,15.2,1.1,2.0,0.21,0.09,0.09)
    gw151226_Mc_mean,gw151226_Mc_std,gw151226_q_mean,gw151226_q_std,gw151226_z_mean,gw151226_z_std\
        = estimateBounds(13.7,3.2,8.8,7.7,2.6,2.2,8.9,0.3,0.3,0.09,0.04,0.04)
    gw170104_Mc_mean,gw170104_Mc_std,gw170104_q_mean,gw170104_q_std,gw170104_z_mean,gw170104_z_std\
        = estimateBounds(31.0,5.6,7.2,20.1,4.5,4.9,21.5,1.7,2.1,0.19,0.08,0.07)
    gw170814_Mc_mean,gw170814_Mc_std,gw170814_q_mean,gw170814_q_std,gw170814_z_mean,gw170814_z_std\
        = estimateBounds(30.7,3.0,5.7,25.3,4.1,2.9,24.2,1.1,1.4,0.12,0.04,0.03)
    gw170608_Mc_mean,gw170608_Mc_std,gw170608_q_mean,gw170608_q_std,gw170608_z_mean,gw170608_z_std\
        = estimateBounds(10.9,1.7,5.3,7.6,2.1,1.3,7.9,0.2,0.2,0.07,0.02,0.02)

    m1_samples,m2_samples,z_samples = makePosterior(\
        gw150914_Mc_mean,gw150914_Mc_std,\
        gw150914_q_mean,gw150914_q_std,\
        gw150914_z_mean,gw150914_z_std,\
        1000)

    m1_samples_151012,m2_samples_151012,z_samples_151012 = makePosterior(\
        gw151012_Mc_mean,gw151012_Mc_std,\
        gw151012_q_mean,gw151012_q_std,\
        gw151012_z_mean,gw151012_z_std,\
        1000)

    m1_samples_151226,m2_samples_151226,z_samples_151226 = makePosterior(\
        gw151226_Mc_mean,gw151226_Mc_std,\
        gw151226_q_mean,gw151226_q_std,\
        gw151226_z_mean,gw151226_z_std,\
        1000)

    m1_samples_170104,m2_samples_170104,z_samples_170104 = makePosterior(\
        gw170104_Mc_mean,gw170104_Mc_std,\
        gw170104_q_mean,gw170104_q_std,\
        gw170104_z_mean,gw170104_z_std,\
        1000)

    m1_samples_170814,m2_samples_170814,z_samples_170814 = makePosterior(\
        gw170814_Mc_mean,gw170814_Mc_std,\
        gw170814_q_mean,gw170814_q_std,\
        gw170814_z_mean,gw170814_z_std,\
        1000)

    m1_samples_170608,m2_samples_170608,z_samples_170608 = makePosterior(\
        gw170608_Mc_mean,gw170608_Mc_std,\
        gw170608_q_mean,gw170608_q_std,\
        gw170608_z_mean,gw170608_z_std,\
        1000)

    a_vals = np.linspace(-4.,5.,10)
    lmbda_vals = np.linspace(-50.,30.,10)
    postProb = np.zeros((a_vals.size,lmbda_vals.size))
    for i in range(a_vals.size):
        for j in range(lmbda_vals.size):
            postProb[i,j] = hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples,z_samples)\
                + hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples_151012,z_samples_151012) \
                + hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples_151226,z_samples_151226) \
                + hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples_170104,z_samples_170104) \
                + hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples_170814,z_samples_170814) \
                + hyperPosterior(a_vals[i],lmbda_vals[j],m1_samples_170608,z_samples_170608) \
                - 6.*np.log(detectionFractionFull(mMin,mMax,a_vals[i],lmbda_vals[j]))
            print(i,j,a_vals[i],postProb[i,j])

    postProb = postProb - np.max(postProb)
    print(postProb)
    postProb = np.exp(postProb)
    print(postProb)
    postProb = postProb/np.sum(postProb)
    print(postProb)
    np.save('posterior.npy',postProb)

    fig,ax = plt.subplots(figsize=(4,3))
    ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    ax.set_xlabel(r'$\alpha$',fontsize=10)
    ax.set_ylabel(r'$\lambda$',fontsize=10)
    ax.xaxis.grid(True,which='major',ls=':')
    ax.yaxis.grid(True,which='major',ls=':')
    ax.set_xlim([-3,4])
    ax.set_ylim(-40,5)
    plt.tight_layout()
    ax.tick_params(labelsize=8)
    plt.savefig("./redshiftFit_Grid.pdf")

