import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

def makePosterior(Mc_mean,Mc_std,q_mean,q_std,z_mean,z_std,nsamples):

    # Chirp mass samples
    # CAUTION: These are samples in *detector frame* chirp mass
    detMc_samples = np.random.normal(loc=Mc_mean,scale=Mc_std,size=nsamples)

    # Convert q bounds to symmetric mass ratio, draw samples
    eta_mean = q_mean/np.power(1.+q_mean,2.)
    eta_high = (q_mean+q_std)/np.power(1.+(q_mean+q_std),2.)
    eta_low = (q_mean-q_std)/np.power(1.+(q_mean-q_std),2.)
    eta_std = (eta_high-eta_low)/2.
    eta_samples = np.random.normal(loc=eta_mean,scale=eta_std,size=nsamples)

    # Fix unphysical values
    while any((eta_samples<0.0) | (eta_samples>0.25)):
        mask = (eta_samples<0.0) | (eta_samples>0.25)
        eta_samples[mask] = np.random.normal(loc=eta_mean,scale=eta_std,size=eta_samples[mask].size)

    # Convert to samples on standard mass ratio
    q_samples = (1.-2*eta_samples-np.sqrt(1.-4*eta_samples))/(2.*eta_samples)

    # Form samples on *detector frame* component masses
    detm1_samples = np.power(q_samples,2./5.)*np.power(1.+q_samples,1./5.)*detMc_samples
    detm2_samples = np.power(q_samples,-3./5.)*np.power(1.+q_samples,1./5.)*detMc_samples

    # Draw redshift samples and convert to source frame masses
    z_samples = np.random.normal(loc=z_mean,scale=z_std,size=nsamples)
    m1_samples = detm1_samples/(1.+z_samples)
    m2_samples = detm2_samples/(1.+z_samples)

    return m1_samples,m2_samples,z_samples

if __name__=="__main__":

    gw150914_Mc_mean = 30.2
    gw150914_Mc_std = (2.5+1.9)/2.
    gw150914_q_mean = 0.79
    gw150914_q_std = (0.18+0.19)/2.
    gw150914_z_mean = 0.083
    gw150914_z_std = (0.033+0.036)/2.

    m1_samples,m2_samples,z_samples = makePosterior(\
        gw150914_Mc_mean,gw150914_Mc_std,\
        gw150914_q_mean,gw150914_q_std,\
        gw150914_z_mean,gw150914_z_std,\
        1000)

    fig = plt.figure(figsize=(16,12))
    ax00 = fig.add_subplot(3,3,1)
    ax10 = fig.add_subplot(3,3,4)
    ax11 = fig.add_subplot(3,3,5)
    ax20 = fig.add_subplot(3,3,7)
    ax21 = fig.add_subplot(3,3,8)
    ax22 = fig.add_subplot(3,3,9)
    ax00.hist(m1_samples)
    ax11.hist(m2_samples)
    ax22.hist(z_samples)
    ax10.hexbin(m1_samples,m2_samples,cmap='Blues',gridsize=20)
    ax20.hexbin(m1_samples,z_samples,cmap='Blues',gridsize=20)
    ax21.hexbin(m2_samples,z_samples,cmap='Blues',gridsize=20)
    plt.savefig('examplePosterior.pdf',bbox_inches='tight')
