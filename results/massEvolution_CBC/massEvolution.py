import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_massEvolution
from scipy.interpolate import LinearNDInterpolator
import matplotlib as mpl
mpl.use('Agg')
from matplotlib import rc
import matplotlib.pyplot as plt

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.
year = 3600.*24*365.

# Load samples
data = np.load('samples.npy')
gamma1 = data[:,0]
gamma2 = data[:,1]
mMax = data[:,2]
alpha = data[:,2]

zs = np.arange(0.,3.,0.05)
fig,ax = plt.subplots(figsize=(4,3))
for i in range(gamma1.size):
    ax.plot(zs,mMax[i]*np.power(1.+zs,gamma2[i]),alpha=0.1,color='black',lw=0.3)
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_xlabel(r'$z$',fontsize=12)
ax.set_ylabel(r'$M_\mathrm{max}$',fontsize=12)
ax.tick_params(axis='both', which='both', labelsize=10, direction='in')
ax.set_ylim([10,1e4])
ax.set_yscale('log')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('Mmax.pdf',bbox_inches='tight')



#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity.pdf')

# Get credible intervals
