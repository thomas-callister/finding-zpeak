import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
import matplotlib.pyplot as plt
import numpy as np

data = np.load('samples.npy')
gamma1 = data[:,0]
gamma2 = data[:,1]
mMax = data[:,2]
alpha = data[:,3]

print(np.mean(gamma1))
print(np.mean(gamma2))
print(np.mean(mMax))
print(np.mean(alpha))

fig = plt.figure(figsize=(12,9))

ax_g1 = fig.add_subplot(441)
ax_g1.hist(gamma1,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g1.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1.set_axisbelow(True)
plt.setp(ax_g1.get_xticklabels(), visible=False)

ax_g1g2 = fig.add_subplot(445,sharex=ax_g1)
ax_g1g2.hexbin(gamma1,gamma2,cmap='Blues',gridsize=15)
ax_g1g2.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1g2.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1g2.set_ylabel(r'$\gamma_2$',fontsize=16)
plt.setp(ax_g1g2.get_xticklabels(), visible=False)

ax_g1m = fig.add_subplot(449,sharex=ax_g1)
ax_g1m.hexbin(gamma1,mMax,cmap='Blues',gridsize=15)
ax_g1m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1m.set_ylabel(r'$M_\mathrm{Max}$',fontsize=16)
plt.setp(ax_g1m.get_xticklabels(), visible=False)

ax_g1a = fig.add_subplot(4,4,13,sharex=ax_g1)
ax_g1a.hexbin(gamma1,alpha,cmap='Blues',gridsize=15)
ax_g1a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1a.set_xlabel(r'$\gamma_1$',fontsize=16)
ax_g1a.set_ylabel(r'$\alpha$',fontsize=16)

ax_g2 = fig.add_subplot(446)
ax_g2.hist(gamma2,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g2.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g2.set_axisbelow(True)
plt.setp(ax_g2.get_xticklabels(), visible=False)
plt.setp(ax_g2.get_yticklabels(), visible=False)

ax_g2m = fig.add_subplot(4,4,10,sharex=ax_g2)
ax_g2m.hexbin(gamma2,mMax,cmap='Blues',gridsize=15)
ax_g2m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2m.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_g2m.get_xticklabels(), visible=False)
plt.setp(ax_g2m.get_yticklabels(), visible=False)

ax_g2a = fig.add_subplot(4,4,14,sharex=ax_g2)
ax_g2a.hexbin(gamma2,alpha,cmap='Blues',gridsize=15)
ax_g2a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g2a.set_xlabel(r'$\gamma_2$',fontsize=16)
plt.setp(ax_g2m.get_xticklabels(), visible=False)
plt.setp(ax_g2m.get_yticklabels(), visible=False)

ax_m = fig.add_subplot(4,4,11)
ax_m.hist(mMax,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_m.set_axisbelow(True)
plt.setp(ax_m.get_xticklabels(), visible=False)
plt.setp(ax_m.get_yticklabels(), visible=False)

ax_ma = fig.add_subplot(4,4,15,sharex=ax_m)
ax_ma.hexbin(mMax,alpha,cmap='Blues',gridsize=15)
ax_ma.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_ma.get_yticklabels(), visible=False)
ax_ma.set_xlabel(r'$M_\mathrm{Max}$',fontsize=16)

ax_a = fig.add_subplot(4,4,16)
ax_a.hist(alpha,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_yticklabels(), visible=False)
ax_a.set_xlabel(r'$\lambda$',fontsize=16)

plt.tight_layout()
plt.savefig('corner.pdf',bbox_inches='tight')


