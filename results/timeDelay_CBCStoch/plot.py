import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
import matplotlib.pyplot as plt
import numpy as np

data = np.load('samples.npy')
gamma = data[:,0]
mMax = data[:,1]
lmbda = data[:,2]

fig = plt.figure(figsize=(12,9))

ax_g = fig.add_subplot(331)
ax_g.hist(gamma,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g.set_axisbelow(True)
plt.setp(ax_g.get_xticklabels(), visible=False)

ax_gm = fig.add_subplot(334,sharex=ax_g)
ax_gm.hexbin(gamma,mMax,cmap='Blues',gridsize=15)
ax_gm.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.set_ylabel(r'$M_\mathrm{Max}$',fontsize=16)
plt.setp(ax_gm.get_xticklabels(), visible=False)

ax_gl = fig.add_subplot(337,sharex=ax_g)
ax_gl.hexbin(gamma,lmbda,cmap='Blues',gridsize=15)
ax_gl.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gl.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gl.set_xlabel(r'$\gamma$',fontsize=16)
ax_gl.set_ylabel(r'$\lambda$',fontsize=16)

ax_m = fig.add_subplot(335)
ax_m.hist(mMax,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_m.set_axisbelow(True)
plt.setp(ax_m.get_xticklabels(), visible=False)
plt.setp(ax_m.get_yticklabels(), visible=False)

ax_ml = fig.add_subplot(338,sharex=ax_m)
ax_ml.hexbin(mMax,lmbda,cmap='Blues',gridsize=15)
ax_ml.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ml.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_ml.get_yticklabels(), visible=False)
ax_ml.set_xlabel(r'$M_\mathrm{Max}$',fontsize=16)

ax_l = fig.add_subplot(339)
ax_l.hist(lmbda,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_l.xaxis.grid(True,which='major',ls=':',color='grey')
ax_l.yaxis.grid(True,which='major',ls=':',color='grey')
ax_l.set_axisbelow(True)
plt.setp(ax_l.get_yticklabels(), visible=False)
ax_l.set_xlabel(r'$\lambda$',fontsize=16)

plt.tight_layout()
plt.savefig('corner.pdf',bbox_inches='tight')


