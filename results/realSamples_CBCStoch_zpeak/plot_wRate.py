import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
rates = np.log10(data[:,5])
print(zpeak.size)

rmin=0.
rmax=2.4
amin=-25.
amax=25.
bmin=0.
bmax=10.
zmin=0.
zmax=4.

blue='#1f78b4'

fig = plt.figure(figsize=(10,7.5))

ax_r = fig.add_subplot(4,4,1)
ax_r.hist(rates,bins=15,color=blue,normed=True,histtype='stepfilled',edgecolor='black')
ax_r.xaxis.grid(True,which='major',ls=':',color='grey')
ax_r.yaxis.grid(True,which='major',ls=':',color='grey')
ax_r.tick_params(axis='both', which='major', labelsize=10)
ax_r.set_axisbelow(True)
plt.setp(ax_r.get_xticklabels(), visible=False)
plt.setp(ax_r.get_yticklabels(), visible=False)

ax_ra = fig.add_subplot(4,4,5,sharex=ax_r)
ax_ra.hexbin(rates,alpha,cmap='Blues',gridsize=15)
ax_ra.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ra.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ra.tick_params(axis='both', which='major', labelsize=10)
ax_ra.set_ylabel(r'$\alpha$',fontsize=12)
plt.setp(ax_ra.get_xticklabels(), visible=False)
ax_ra.set_ylim(amin,amax)

ax_rb = fig.add_subplot(4,4,9,sharex=ax_r)
ax_rb.hexbin(rates,beta,cmap='Blues',gridsize=15)
ax_rb.xaxis.grid(True,which='major',ls=':',color='grey')
ax_rb.yaxis.grid(True,which='major',ls=':',color='grey')
ax_rb.tick_params(axis='both', which='major', labelsize=10)
ax_rb.set_ylabel(r'$\beta$',fontsize=12)
plt.setp(ax_rb.get_xticklabels(), visible=False)
ax_rb.set_ylim(bmin,bmax)

ax_rz = fig.add_subplot(4,4,13,sharex=ax_r)
ax_rz.hexbin(rates,zpeak,cmap='Blues',gridsize=15)
ax_rz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_rz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_rz.tick_params(axis='both', which='major', labelsize=10)
ax_rz.set_xlabel(r'$\mathcal{R}_0\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax_rz.set_ylabel(r'$z_\mathrm{peak}$',fontsize=12)
ax_rz.set_xticks([0,0.5,1,1.5,2])
ax_rz.set_xticklabels(['$10^{0.0}$','$10^{0.5}$','$10^{1.0}$','$10^{1.5}$','$10^{2.0}$'])
ax_rz.set_xlim(rmin,rmax)
ax_rz.set_ylim(zmin,zmax)

ax_a = fig.add_subplot(4,4,6)
ax_a.hist(alpha,bins=15,color=blue,normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.tick_params(axis='both', which='major', labelsize=10)
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_xticklabels(), visible=False)
plt.setp(ax_a.get_yticklabels(), visible=False)

ax_ab = fig.add_subplot(4,4,10,sharex=ax_a)
ax_ab.hexbin(alpha,beta,cmap='Blues',gridsize=15)
ax_ab.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.tick_params(axis='both', which='major', labelsize=10)
plt.setp(ax_ab.get_xticklabels(), visible=False)
plt.setp(ax_ab.get_yticklabels(), visible=False)
ax_ab.set_ylim(bmin,bmax)

ax_az = fig.add_subplot(4,4,14,sharex=ax_a)
ax_az.hexbin(alpha,zpeak,cmap='Blues',gridsize=15)
ax_az.xaxis.grid(True,which='major',ls=':',color='grey')
ax_az.yaxis.grid(True,which='major',ls=':',color='grey')
ax_az.tick_params(axis='both', which='major', labelsize=10)
ax_az.set_xlabel(r'$\alpha$',fontsize=12)
plt.setp(ax_az.get_yticklabels(), visible=False)
ax_az.set_xlim(amin,amax)
ax_az.set_ylim(zmin,zmax)

ax_b = fig.add_subplot(4,4,11)
ax_b.hist(beta,bins=15,color=blue,normed=True,histtype='stepfilled',edgecolor='black')
ax_b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_b.tick_params(axis='both', which='major', labelsize=10)
ax_b.set_axisbelow(True)
plt.setp(ax_b.get_xticklabels(), visible=False)
plt.setp(ax_b.get_yticklabels(), visible=False)

ax_bz = fig.add_subplot(4,4,15,sharex=ax_b)
ax_bz.hexbin(beta,zpeak,cmap='Blues',gridsize=15)
ax_bz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.tick_params(axis='both', which='major', labelsize=10)
ax_bz.set_xlabel(r'$\beta$',fontsize=12)
plt.setp(ax_bz.get_yticklabels(), visible=False)
ax_bz.set_xlim(bmin,bmax)
ax_bz.set_ylim(zmin,zmax)

ax_z = fig.add_subplot(4,4,16)
ax_z.hist(zpeak,bins=15,color=blue,normed=True,histtype='stepfilled',edgecolor='black')
ax_z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_z.tick_params(axis='both', which='major', labelsize=10)
ax_z.set_axisbelow(True)
ax_z.set_xlabel(r'$z_\mathrm{peak}$',fontsize=12)
plt.setp(ax_z.get_yticklabels(), visible=False)
ax_z.set_xlim(zmin,zmax)

plt.tight_layout()
plt.savefig('corner_restricted_wRate.pdf',bbox_inches='tight')


