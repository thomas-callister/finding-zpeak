import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../../code/')
from loadSamples import loadSamples
from gwBackground import *
from detectionFraction import detectionFractionMaya_extended
from detectionFraction import detectionFractionMaya

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 7.
gamma_Min = -4.
gamma_Max = 12.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.
zmax = 6.
N_Min = 10.**0.5
N_Max = 10.**30.

# BBH data
Tobs = 169.7/365.25
m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("./../../GWTC-1_sample_release/GW150914_GWTC-1.hdf5")
m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012 = loadSamples("./../../GWTC-1_sample_release/GW151012_GWTC-1.hdf5")
m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226 = loadSamples("./../../GWTC-1_sample_release/GW151226_GWTC-1.hdf5")
m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104 = loadSamples("./../../GWTC-1_sample_release/GW170104_GWTC-1.hdf5")
m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608 = loadSamples("./../../GWTC-1_sample_release/GW170608_GWTC-1.hdf5")
m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729 = loadSamples("./../../GWTC-1_sample_release/GW170729_GWTC-1.hdf5")
m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809 = loadSamples("./../../GWTC-1_sample_release/GW170809_GWTC-1.hdf5")
m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814 = loadSamples("./../../GWTC-1_sample_release/GW170814_GWTC-1.hdf5")
m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818 = loadSamples("./../../GWTC-1_sample_release/GW170818_GWTC-1.hdf5")
m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823 = loadSamples("./../../GWTC-1_sample_release/GW170823_GWTC-1.hdf5")

sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170729,z_samples_170729,priors_170729]),\
    6:np.array([m1_samples_170809,z_samples_170809,priors_170809]),\
    7:np.array([m1_samples_170814,z_samples_170814,priors_170814]),\
    8:np.array([m1_samples_170818,z_samples_170818,priors_170818]),\
    9:np.array([m1_samples_170823,z_samples_170823,priors_170823]),\
    }

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)

# Import stochastic data
freqs_O1,C_O1,sigma_O1 = np.loadtxt('Cf_O1.dat',unpack=True,skiprows=1)
freqs_O2,C_O2,sigma_O2 = np.loadtxt('Cf_O2.dat',unpack=True,skiprows=1)

# Combine
C_stoch = (C_O1/sigma_O1**2. + C_O2/sigma_O2**2.)/(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma_stoch = 1./np.sqrt(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma2_stoch = np.power(sigma_stoch,2.)
freqs_stoch = freqs_O2
np.savetxt("Cf_combined.dat",np.transpose([freqs_O1,C_stoch,sigma_stoch]))

# Select frequencies below 300 Hz
lowFreqs = freqs_stoch<300.
freqs_stoch = freqs_stoch[lowFreqs]
C_stoch = C_stoch[lowFreqs]
sigma2_stoch = sigma2_stoch[lowFreqs]

# Select only frequencies with data
goodInds = np.where(C_stoch==C_stoch)
freqs_stoch = freqs_stoch[goodInds]
C_stoch = C_stoch[goodInds]
sigma2_stoch = sigma2_stoch[goodInds]

def loglikelihood(c):

    # Read parameters
    gamma = c[0]
    mMax = c[1]
    alpha = c[2]
    beta = c[3]
    zpeak = c[4]

    if gamma<gamma_Min or gamma>gamma_Max or mMax<mMax_Min or mMax>mMax_Max or alpha<alpha_Min or alpha>alpha_Max or beta<beta_Min or beta>beta_Max or zpeak<zpeak_Min or zpeak>zpeak_Max:
        return -np.inf

    else:
        logP = 0.

        # Construct redshift probability distribution
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,alpha-1.)/(1.+np.power((1.+ref_zs)/(1.+zpeak),alpha+beta))
        dz = ref_zs[1] - ref_zs[0]
        ref_pzs /= np.sum(ref_pzs[ref_zs<0.9])*dz

        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]
            
            # Mass 1 probability
            if gamma==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-gamma)*np.power(m1_samples,-gamma)/(np.power(mMax,1.-gamma)-np.power(mMin,1.-gamma))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above mMax
            p_m1[m1_samples>mMax] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        # Stochastic energy-density spectrum
        OmgGW = OmegaGW_massDistr(alpha,beta,zpeak,gamma,mMax,freqs_stoch,freqInterp=True)

        # Stochastic likelihood
        logP_stoch = np.sum(-0.5*np.power(OmgGW-C_stoch,2.)/sigma2_stoch)
        logP += logP_stoch

        print(mMax,gamma,alpha,beta,zpeak,OmgGW[0],logP)

        return logP - 10.*np.log(detectionFractionMaya_extended(mMin,mMax,gamma,alpha,beta,zpeak,nTrials=3000,znorm=True))


if __name__=="__main__":

    # Initialize walkers
    nWalkers = 16
    initial_gammas = np.random.random(nWalkers)*(gamma_Max-gamma_Min)+gamma_Min
    initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    initial_alphas = np.random.random(nWalkers)*(alpha_Max-alpha_Min)+alpha_Min
    initial_betas = np.random.random(nWalkers)*(beta_Max-beta_Min)+beta_Min
    initial_zpeaks = np.random.random(nWalkers)*(zpeak_Max-zpeak_Min)+zpeak_Min
    initial_walkers = np.transpose([initial_gammas,initial_mMaxs,initial_alphas,initial_betas,initial_zpeaks])

    # Run
    nSteps = 3000
    sampler = mc.EnsembleSampler(nWalkers,5,loglikelihood,threads=6)
    sampler.run_mcmc(initial_walkers,nSteps)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]
    print(chainBurned)

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(5):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*5)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print(chainDownsampled)
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples2.npy',chainDownsampled)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

