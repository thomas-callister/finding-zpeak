import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../../code/')
from loadSamples import loadSamples
from gwBackground import *
from detectionFraction import detectionFractionMaya_extended
from detectionFraction import detectionFractionMaya

if __name__=="__main__":

    nSteps = 1000
    nWalkers = 16
    chain = np.load('samplesFull.npy')[()]

    # Burn first half
    chainBurned = chain[:,int(np.floor(nSteps/2.)):,:]
    print(chainBurned)

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(6):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*6)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print(chainDownsampled)
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples.npy',chainDownsampled)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

