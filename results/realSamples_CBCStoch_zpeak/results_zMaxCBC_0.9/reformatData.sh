#!/bin/bash

# stochastic.m files containing point-estimate integrands and sensitivity integrands
# O2
O2datadir=/home/rich.ormiston/iso/O2/Zero_Lag_O2/pproc/a0
O2ptEstFile=$O2datadir/H1L1_ptEstIntegrand.dat
O2sensIntFile=$O2datadir/H1L1_sensIntegrand.dat

O1datadir=/home/thomas.callister/Stochastic/stochastic/trunk/analyses/O1/isotropic/output/postProcessing_output/a0
O1ptEstFile=$O1datadir/H1L1_ptEstIntegrand.dat
O1sensIntFile=$O1datadir/H1L1_sensIntegrand.dat

# Target file in which to save Y(f) and sigma(f)
O2target=./H1L1_narrowbandSpectra_O2.dat
O1target=./H1L1_narrowbandSpectra_O1.dat

# Other quantities
h0=0.68		# Hubble parameter
dt=192.0	# Stochastic.m segment duration (used for bias factor correction)
df=0.03125	# Stochastic.m frequency resolution (bias correction)

# Run
codeDir=~/Stochastic/stochastic/trunk/analyses/O2/iso_postProcessing/
python $codeDir/reformatStochasticData.py -ptEst $O2ptEstFile -sensInts $O2sensIntFile -t $O2target\
	-h0 $h0 -correctBias -dt $dt -df $df
python $codeDir/reformatStochasticData.py -ptEst $O1ptEstFile -sensInts $O1sensIntFile -t $O1target\
	-h0 $h0 -correctBias -dt $dt -df $df
