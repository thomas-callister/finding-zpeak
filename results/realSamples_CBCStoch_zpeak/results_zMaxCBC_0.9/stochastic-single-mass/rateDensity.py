import numpy as np
import os
import sys
sys.path.append('./../../../code/')
from detectionFraction import detectionFractionMaya_extended
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.

# Load samples
data = np.load('samples.npy')
#data2 = np.load('samples_z4.npy')
#gamma = np.append(data[:,0],data2[:,1])
#mMax = np.append(data[:,1],data2[:,1])
#alpha = np.append(data[:,2],data2[:,2])
#beta = np.append(data[:,3],data2[:,3])
#zpeak = np.append(data[:,4],data2[:,4])
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
print(zpeak.size)

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<0.9
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

totalRates = np.zeros(alpha.size)
localRates = np.zeros(alpha.size)
#totalRates = np.zeros(100)
#localRates = np.zeros(100)
print(alpha.size)

# Loop across samples
for i in range(alpha.size):
#for i in range(100):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_extended(7.,mMax[i],gamma[i],alpha[i],beta[i],zpeak[i],nTrials=10000,znorm=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(0.5,30,1000)
    #dlnN = np.log(Ntots[1]*dFraction) - np.log(Ntots[0]*dFraction)
    dlnN = np.log(Ntots[1]) - np.log(Ntots[0])
    p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
    p_Ns /= np.sum(p_Ns)*dlnN
    cdf_Ns = np.cumsum(p_Ns)*dlnN

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha[i]-1)*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+ref_zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))


#fig,ax = plt.subplots(figsize=(4,3))
#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity.pdf')

localRatesFinite = localRates[localRates!=0]


fig,ax = plt.subplots(figsize=(4,3))
for i in range(alpha.size):   
#for i in range(100):   
    if localRates[i]!=0:
        zs = np.arange(0.,3.,0.05)
        ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.1,color='black',lw=0.3)
ax.set_yscale('log')
ax.set_xlim([0,3])
ax.set_ylim([1e-4,1e8])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel('Rate per Comov. Volume')
ax.set_xlabel('Redshift')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate_test.pdf',bbox_inches='tight')
