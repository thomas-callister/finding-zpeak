import numpy as np
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../code/')
from loadSamples import loadSamples
from gwBackground import OmegaGW
from scipy.ndimage import gaussian_filter

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 7.
gamma_Min = -4.
gamma_Max = 12.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.
zmax = 6.

# Import stochastic data
freqs_O1,C_O1,sigma_O1 = np.loadtxt('Cf_O1.dat',unpack=True,skiprows=1)
freqs_O2,C_O2,sigma_O2 = np.loadtxt('Cf_O2.dat',unpack=True,skiprows=1)

# Combine
C_stoch = (C_O1/sigma_O1**2. + C_O2/sigma_O2**2.)/(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma_stoch = 1./np.sqrt(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma2_stoch = np.power(sigma_stoch,2.)
freqs_stoch = freqs_O2
np.savetxt("Cf_combined.dat",np.transpose([freqs_O1,C_stoch,sigma_stoch]))

# Select frequencies below 300 Hz
lowFreqs = freqs_stoch<300.
freqs_stoch = freqs_stoch[lowFreqs]
C_stoch = C_stoch[lowFreqs]
sigma2_stoch = sigma2_stoch[lowFreqs]

# Select only frequencies with data
goodInds = np.where(C_stoch==C_stoch)
freqs_stoch = freqs_stoch[goodInds]
C_stoch = C_stoch[goodInds]
sigma2_stoch = sigma2_stoch[goodInds]

def snr(alpha,zpeak):

    # Stochastic energy-density spectrum
    beta = 3.
    OmgGW = OmegaGW(alpha,beta,zpeak,freqs_stoch)

    return np.sqrt(np.sum(np.power(OmgGW,2.)/sigma2_stoch))

if __name__=="__main__":

    alphas = np.linspace(-25,25,50)
    zpeaks = np.linspace(0.,5.,50)
    snrs = np.zeros((alphas.size,zpeaks.size))

    for i,a in enumerate(alphas):
        for j,z in enumerate(zpeaks):
            snrs[i,j] = snr(a,z)
            print(a,z,snrs[i,j])

    fig,ax = plt.subplots(figsize=(4,3))
    pc = ax.pcolormesh(alphas,zpeaks,np.log10(snrs.T),cmap='Blues',vmin=-2,vmax=3)
    cs = ax.contour(alphas,zpeaks,snrs.T,levels=(3,10),colors='black')
    ax.clabel(cs,inline=1,fontsize=10,fmt='%d',manual=[(3,1),(11,1)])
    ax.xaxis.grid(True,which='major',ls=':',alpha=0.3)
    ax.yaxis.grid(True,which='major',ls=':',alpha=0.3)
    ax.set_xlabel(r'$\alpha$',fontsize=12)
    ax.set_ylabel(r'$z_\mathrm{peak}$',fontsize=12)
    ax.tick_params(labelsize=10)

    cbar = fig.colorbar(pc,ax=ax)
    cbar.set_label(label='Stochastic SNR',size=12)
    cbar.ax.tick_params(labelsize=10)
    cbar_labels = [item.get_text() for item in cbar.ax.get_yticklabels()]
    print([int(x[1:-1]) for x in cbar_labels])
    cbar_labels = [r"$10^{{{0}}}$".format(int(x[1:-1])) for x in cbar_labels]
    cbar.ax.set_yticklabels(cbar_labels)

    plt.tight_layout()
    plt.savefig('snrs_R20.pdf',bbox_inches='tight')
