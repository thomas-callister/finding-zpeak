import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_extended
from gwBackground import OmegaGW_massDistr
from scipy.interpolate import LinearNDInterpolator
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.style
matplotlib.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')
from matplotlib import rc
import matplotlib.pyplot as plt

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.
year = 3600.*24*365.

# Load samples
data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
localRates = data[:,5]

# Import redshift data (all in units of Gpc) and construct probability distribution values
"""
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<1.5
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

totalRates = np.zeros(alpha.size)
localRates = np.zeros(alpha.size)
print(alpha.size)

# Loop across samples
for i in range(alpha.size):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_extended(7.,mMax[i],gamma[i],alpha[i],beta[i],zpeak[i],nTrials=3000,znorm=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(0.5,20,1000)
    p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha[i]-1)*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+ref_zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))

np.save('localRates.npy',localRates)
localRates = np.load('localRates.npy')
"""

# Get credible intervals
zs = np.arange(0.,5.1,0.1)
rateArray = np.zeros((localRates.size,zs.size))
for i in range(localRates.size):
    rateArray[i,:] = localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
z_2sigma_low = np.quantile(rateArray,0.025,axis=0)
z_1sigma_low = np.quantile(rateArray,0.26,axis=0)
z_1sigma_high = np.quantile(rateArray,0.84,axis=0)
z_2sigma_high = np.quantile(rateArray,0.975,axis=0)

# Merger rate plot
fig,ax = plt.subplots(figsize=(5,3))
for i in range(alpha.size):   
    if localRates[i]!=0:
        ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.2,color='#1f78b4',lw=0.4,zorder=0)
ax.plot(zs,z_2sigma_low,color='#525252',lw=1.,alpha=0.8)
ax.plot(zs,z_2sigma_high,color='#525252',lw=1.,alpha=0.8)
ax.plot(zs,z_1sigma_low,color='#525252',ls='--',lw=1.,alpha=0.8)
ax.plot(zs,z_1sigma_high,color='#525252',ls='--',lw=1.,alpha=0.8)
ax.set_yscale('log')
ax.set_xlim([0,5])
ax.set_ylim([1e-4,1e8])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate.pdf',bbox_inches='tight')


# SGWB
freqs = 10.**np.arange(1.,3.,0.01)
fig,ax = plt.subplots(figsize=(4,3))

pi_freqs,pi_values = np.loadtxt('Figures_3_and_4_PICurve_O1O2.dat',unpack=True,skiprows=1,usecols=(0,2))

"""
OmgArray = np.zeros((alpha.size,freqs.size))
for i in range(alpha.size):
    print(i)
    OmgArray[i,:] = OmegaGW_massDistr(localRates[i]/year/1.e9,alpha[i],beta[i],zpeak[i],gamma[i],mMax[i],freqs)
np.save('OmgGWdata.npy',OmgArray)
"""
OmgArray = np.load('OmgGWdata.npy')

Omg_95 = np.quantile(OmgArray,0.95,axis=0)
Omg_50 = np.quantile(OmgArray,0.50,axis=0)
print(np.interp(25.,freqs,Omg_50))

for i in range(alpha.size):
    ax.plot(freqs,OmgArray[i,:],lw=0.25,alpha=0.2,color='#ef3b2c',zorder=0)
ax.plot(pi_freqs,2.*pi_values,lw=1.,color='#1f78b4',ls='--',label=r'$2\sigma$ O1+O2 PI Curve')
ax.plot(freqs,Omg_95,lw=1.,alpha=0.8,color='black',label=r'$95\%$ Credible Upper Limit',ls='--')
ax.plot(freqs,Omg_50,lw=1.,alpha=0.8,color='black',label=r'Median')
plt.legend(loc='lower left',fontsize=10,frameon=False)
ax.set_yscale('log')
ax.set_xscale('log')
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_xlabel(r'$f\,(\mathrm{Hz})$',fontsize=12)
ax.set_ylabel(r'$\Omega(f)$',fontsize=12)
ax.tick_params(axis='both', which='both', labelsize=10, direction='in')
ax.set_xlim(10,1000)
ax.set_ylim(1e-12,1e-6)
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('stochasticBackground.pdf',bbox_inches='tight')
