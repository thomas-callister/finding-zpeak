import numpy as np

def interval95(samples):

    samples = np.sort(samples)
    med = np.median(samples)
    lowerBound = samples[int(0.025*samples.size)]
    upperBound = samples[int(0.975*samples.size)]
    lowerError = med-lowerBound
    upperError = upperBound-med

    return med,upperError,lowerError

def upperLim(samples):

    samples = np.sort(samples)
    return samples[int(0.95*samples.size)]


#localRates = np.load('localRates.npy')
#data = np.load('samples.npy')
data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
localRates = data[:,5]

print("Rate:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(localRates)))
print("gamma:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(gamma)))
print("mMax:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(mMax)))
print("alpha:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(alpha)))
print("beta:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(beta)))
print("zpeak:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(zpeak)))

print("alpha Lim:\t{0:.2f}".format(upperLim(alpha)))

print("\n")
print("\\newcommand\\DirectStochRateMed{{{0:.1f}}}\n\\newcommand\\DirectStochRateHigh{{{1:.1f}}}\n\\newcommand\\DirectStochRateLow{{{2:.1f}}}".format(*interval95(localRates)))
print("\\newcommand\\DirectStochGammaMed{{{0:.1f}}}\n\\newcommand\\DirectStochGammaHigh{{{1:.1f}}}\n\\newcommand\\DirectStochGammaLow{{{2:.1f}}}".format(*interval95(gamma)))
print("\\newcommand\\DirectStochMmaxMed{{{0:.1f}}}\n\\newcommand\\DirectStochMmaxHigh{{{1:.1f}}}\n\\newcommand\\DirectStochMmaxLow{{{2:.1f}}}".format(*interval95(mMax)))
print("\\newcommand\\DirectStochAlphaMed{{{0:.1f}}}\n\\newcommand\\DirectStochAlphaHigh{{{1:.1f}}}\n\\newcommand\\DirectStochAlphaLow{{{2:.1f}}}".format(*interval95(alpha)))
print("\\newcommand\\DirectStochAlphaUpperLim{{{0:.1f}}}".format(upperLim(alpha)))
