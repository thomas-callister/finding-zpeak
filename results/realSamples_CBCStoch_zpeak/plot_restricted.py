import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

data = np.load('samples.npy')

gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
print(zpeak.size)

fig = plt.figure(figsize=(8,6))

ax_a = fig.add_subplot(3,3,1)
ax_a.hist(alpha,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.tick_params(axis='both', which='major', labelsize=10)
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_xticklabels(), visible=False)
plt.setp(ax_a.get_yticklabels(), visible=False)

ax_ab = fig.add_subplot(3,3,4,sharex=ax_a)
ax_ab.hexbin(alpha,beta,cmap='Blues',gridsize=15)
ax_ab.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.tick_params(axis='both', which='major', labelsize=10)
ax_ab.set_ylabel(r'$\beta$',fontsize=12)
plt.setp(ax_ab.get_xticklabels(), visible=False)

ax_az = fig.add_subplot(3,3,7,sharex=ax_a)
ax_az.hexbin(alpha,zpeak,cmap='Blues',gridsize=15)
ax_az.xaxis.grid(True,which='major',ls=':',color='grey')
ax_az.yaxis.grid(True,which='major',ls=':',color='grey')
ax_az.tick_params(axis='both', which='major', labelsize=10)
ax_az.set_ylim(0,4)
ax_az.set_xlabel(r'$\alpha$',fontsize=12)
ax_az.set_ylabel(r'$z_\mathrm{peak}$',fontsize=12)

ax_b = fig.add_subplot(3,3,5)
ax_b.hist(beta,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_b.tick_params(axis='both', which='major', labelsize=10)
ax_b.set_axisbelow(True)
plt.setp(ax_b.get_xticklabels(), visible=False)
plt.setp(ax_b.get_yticklabels(), visible=False)

ax_bz = fig.add_subplot(3,3,8,sharex=ax_b)
ax_bz.hexbin(beta,zpeak,cmap='Blues',gridsize=15)
ax_bz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.tick_params(axis='both', which='major', labelsize=10)
ax_bz.set_xlabel(r'$\beta$',fontsize=12)
plt.setp(ax_bz.get_yticklabels(), visible=False)

ax_z = fig.add_subplot(3,3,9)
ax_z.hist(zpeak,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_z.tick_params(axis='both', which='major', labelsize=10)
ax_z.set_axisbelow(True)
ax_z.set_xlabel(r'$z_\mathrm{peak}$',fontsize=12)
plt.setp(ax_z.get_yticklabels(), visible=False)

plt.tight_layout()
plt.savefig('corner_restricted.pdf',bbox_inches='tight')


