import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_extended
from scipy.interpolate import LinearNDInterpolator
from matplotlib import rc
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from computeTotalTime import obsTime
from loadMockSamples import *

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

mMin = 5.

# Compute observation time
Nobs = 500
Tobs = obsTime(Nobs,30.,1.2,45.,3.,3.,2.0)
print(Nobs)
print(Tobs)

# Load samples
data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0][::1]
mMax = data[:,1][::1]
alpha = data[:,2][::1]
beta = data[:,3][::1]
zpeak = data[:,4][::1]
localRates = data[:,5][::1]

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<3.
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

"""
# Loop across samples
for i in range(alpha.size):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_extended(5.,mMax[i],gamma[i],alpha[i],beta[i],zpeak[i],nTrials=3000,znorm=True,design=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(3,8,1000)
    p_Ns_tmp = dFraction*Ntots*np.exp(-dFraction*Ntots/Nobs)
    p_Ns = np.power(p_Ns_tmp/np.max(p_Ns_tmp),Nobs)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha[i]-1)*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+ref_zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))

np.save('localRates{0}.npy'.format(run),localRates)
localRates = np.load('localRates{0}.npy'.format(run))
"""

# Get credible intervals
zs = np.arange(0.,5.05,0.05)
rateArray = np.zeros((localRates.size,zs.size))
for i in range(localRates.size):
    rateArray[i,:] = localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
z_2sigma_low = np.quantile(rateArray,0.025,axis=0)
z_1sigma_low = np.quantile(rateArray,0.26,axis=0)
z_1sigma_high = np.quantile(rateArray,0.84,axis=0)
z_2sigma_high = np.quantile(rateArray,0.975,axis=0)

alpha_true = 3.
beta_true = 3.
zpeak_true = 2.0
localRate_true = 30.


fig,ax = plt.subplots(figsize=(4,3))
for i in range(0,alpha.size,2):   
    if localRates[i]!=0:
        ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.2,color='#4daf4a',lw=0.4,zorder=0)
ax.plot(zs,z_2sigma_low,color='#525252',lw=0.7,alpha=0.8)
ax.plot(zs,z_2sigma_high,color='#525252',lw=0.7,alpha=0.8)
ax.plot(zs,z_1sigma_low,color='#525252',ls='--',lw=0.7,alpha=0.8)
ax.plot(zs,z_1sigma_high,color='#525252',ls='--',lw=0.7,alpha=0.8)
ax.plot(zs,localRate_true*np.power(1.+zs,alpha_true)*(1.+np.power(1./(1.+zpeak_true),alpha_true+beta_true))/(1.+np.power((1.+zs)/(1.+zpeak_true),alpha_true+beta_true)),lw=1.,zorder=10,color='black')
ax.set_yscale('log')
ax.set_xlim([0,5])
ax.set_ylim([1e-1,1e5])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate.pdf',bbox_inches='tight')
