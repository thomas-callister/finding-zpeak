import numpy as np

def interval95(samples):

    samples = np.sort(samples)
    med = np.median(samples)
    lowerBound = samples[int(0.025*samples.size)]
    upperBound = samples[int(0.975*samples.size)]
    lowerError = med-lowerBound
    upperError = upperBound-med

    return med,upperError,lowerError

def upperLim(samples):
    samples = np.sort(samples)
    return samples[int(0.95*samples.size)]

def lowerLim(samples):
    samples = np.sort(samples)
    return samples[int(0.05*samples.size)]


#localRates = np.load('localRates.npy')
#data = np.load('samples.npy')
data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
localRates = data[:,5]

print("Rate:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(localRates)))
print("gamma:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(gamma)))
print("mMax:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(mMax)))
print("alpha:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(alpha)))
print("beta:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(beta)))
print("zpeak:\t{0:.2f} +{1:.2f} -{2:.2f}".format(*interval95(zpeak)))

print("alpha Lim:\t{0:.2f}".format(upperLim(alpha)))
print("zpeak Lim:\t{0:.2f}".format(lowerLim(zpeak)))

print("\n")
print("\\newcommand\\DesignDirectRateMed{{{0:.1f}}}\n\\newcommand\\DesignDirectRateHigh{{{1:.1f}}}\n\\newcommand\\DesignDirectRateLow{{{2:.1f}}}".format(*interval95(localRates)))
print("\\newcommand\\DesignDirectGammaMed{{{0:.1f}}}\n\\newcommand\\DesignDirectGammaHigh{{{1:.1f}}}\n\\newcommand\\DesignDirectGammaLow{{{2:.1f}}}".format(*interval95(gamma)))
print("\\newcommand\\DesignDirectMmaxMed{{{0:.1f}}}\n\\newcommand\\DesignDirectMmaxHigh{{{1:.1f}}}\n\\newcommand\\DesignDirectMmaxLow{{{2:.1f}}}".format(*interval95(mMax)))
print("\\newcommand\\DesignDirectAlphaMed{{{0:.1f}}}\n\\newcommand\\DesignDirectAlphaHigh{{{1:.1f}}}\n\\newcommand\\DesignDirectAlphaLow{{{2:.1f}}}".format(*interval95(alpha)))
print("\\newcommand\\DesignDirectZpeakLowerLim{{{0:.1f}}}".format(lowerLim(zpeak)))
