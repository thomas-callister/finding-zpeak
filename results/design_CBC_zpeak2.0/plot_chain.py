import numpy as np
import acor
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import sys

run=sys.argv[1]
print(run)

#data = np.load('raw_samples{0}.npy'.format(run))
data = np.load('raw_samples.npy')
goodInds = np.where(data[0,:,1]!=0)

fig,ax = plt.subplots(figsize=(12,5))
for i in range(0,96):
    ax.plot(data[i,:,4][goodInds],lw=0.75,alpha=0.8)
#plt.savefig('test{0}.pdf'.format(run))
plt.savefig('test.pdf')

nSteps = goodInds[0].size
nWalkers = data[:,0,0].size
nVars = data[0,0,:].size
#print(nSteps)

# Burn first half
chainBurned = data[:,int(np.floor(nSteps/2.)):nSteps,:]
#print(chainBurned.shape)

# Get mean correlation length (averaging over all variables and walkers)
corrTotal = np.zeros(nVars)
for i in range(nVars):
    for j in range(nWalkers):
        try:
            (tau,mean,sigma) = acor.acor(chainBurned[j,:,i])
            print(i,j,tau)
            if tau==0 or tau!=tau:
                tau = 20
        except RuntimeError:
            tau = 20
        corrTotal[i] += 2.*tau/(nWalkers)
        #print(corrTotal,tau)
print(corrTotal)
meanCorLength = np.max(corrTotal)
#meanCorLength = int(np.floor(corrTotal/(nWalkers*4)))
print("Cl:\t",meanCorLength)

# Down-sample by twice the mean correlation length
chainDownsampled = chainBurned[:,::int(meanCorLength),:]
#print(chainDownsampled)
print np.shape(chainDownsampled)

fig,ax = plt.subplots(figsize=(12,5))
for i in range(0,96):
    ax.plot(chainDownsampled[i,:,2],lw=0.75,alpha=0.8)
plt.savefig('test_ds.pdf')


# Flatten
chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
print np.shape(chainDownsampled)
np.save('samples.npy'.format(run),chainDownsampled)

