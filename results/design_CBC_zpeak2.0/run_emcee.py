import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('/home/thomas.callister/RedshiftDistributions/finding-zpeak/code/')
from loadMockSamples import loadMockSamples
from detectionFraction import detectionFractionMaya_extended

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 5.
gamma_Min = -4.
gamma_Max = 12.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.

sampleDict = loadMockSamples('/home/thomas.callister/RedshiftDistributions/finding-zpeak/mock_detections/design_alpha3.0_beta3.0_zpeak2.0/mockevents_design.h5',500)
nEvents = len(sampleDict)
print(nEvents)

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('/home/thomas.callister/RedshiftDistributions/finding-zpeak/code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)

def loglikelihood(c):

    # Read parameters
    gamma = c[0]
    mMax = c[1]
    alpha = c[2]
    beta = c[3]
    zpeak = c[4]

    if gamma<gamma_Min or gamma>gamma_Max or mMax<mMax_Min or mMax>mMax_Max or alpha<alpha_Min or alpha>alpha_Max or beta<beta_Min or beta>beta_Max or zpeak<zpeak_Min or zpeak>zpeak_Max:
        return -np.inf

    else:

        # Construct redshift probability distribution
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,alpha-1.)/(1.+np.power((1.+ref_zs)/(1.+zpeak),alpha+beta))

        logP = 0.
        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]
            
            # Mass 1 probability
            if gamma==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-gamma)*np.power(m1_samples,-gamma)/(np.power(mMax,1.-gamma)-np.power(mMin,1.-gamma))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above mMax and below mMin
            p_m1[m1_samples>mMax] = 0
            p_m1[m1_samples<mMin] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        #print(mMax,gamma,alpha,beta,zpeak,logP)
        return logP - nEvents*np.log(detectionFractionMaya_extended(mMin,mMax,gamma,alpha,beta,zpeak,nTrials=3000,design=True))

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 96
    initial_gammas = np.random.random(nWalkers)*(1.3-1.1)+1.1
    initial_mMaxs = np.random.random(nWalkers)*(48.-43.)+43.
    initial_alphas = np.random.random(nWalkers)*(3.2-2.8)+2.8
    initial_betas = np.random.random(nWalkers)*(beta_Max-beta_Min)+beta_Min
    initial_zpeaks = np.random.random(nWalkers)*(2.0-1.0)+1.0
    initial_walkers = np.transpose([initial_gammas,initial_mMaxs,initial_alphas,initial_betas,initial_zpeaks])
    print(initial_walkers)

    # Run
    nSteps = 8000
    sampler = mc.EnsembleSampler(nWalkers,5,loglikelihood,threads=8)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        if i%10==0:
            np.save('/home/thomas.callister/RedshiftDistributions/finding-zpeak/results/design_CBC_zpeak2.0/raw_samples.npy',sampler.chain)
            print("============= {0} ... {1} ============".format(i,np.mean(sampler.acceptance_fraction)))

    np.save('/home/thomas.callister/RedshiftDistributions/finding-zpeak/results/design_CBC_zpeak2.0/raw_samples.npy',sampler.chain)

