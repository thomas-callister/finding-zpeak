import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_timeDelay
from scipy.interpolate import LinearNDInterpolator
from matplotlib import rc
import matplotlib.pyplot as plt

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.

# Load samples
data = np.load('samples.npy')
gamma = data[:,0]
mMax = data[:,1]
lmbda = data[:,2]

# Import redshift data (all in units of Gpc) and construct probability distribution values
all_zs,ref_dcs,ref_dLs,all_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = all_zs<0.9
ref_zs = all_zs[low_zs]
ref_dVc_dz = all_dVc_dz[low_zs]

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('./../../code/delayedRateData.npy')[()]
formationRates = rateData['formationRates']
tdelays = rateData['tds']
zformation = rateData['zs']

totalRates = np.zeros(lmbda.size)
localRates = np.zeros(lmbda.size)
print(lmbda.size)

# Loop across samples
for i in range(gamma.size):
    print(i)

    # Convolve formation rate with time-delay distribution
    dpdt = np.power(tdelays,lmbda[i])
    dpdt[tdelays<0.05] = 0.
    dpdt[tdelays>13.5] = 0.
    mergerRate = formationRates.dot(dpdt)

    # Construct redshift probability distribution
    all_pzs = all_dVc_dz*mergerRate/(1.+all_zs)
    dz = ref_zs[1] - ref_zs[0]
    all_pzs /= np.sum(all_pzs)*dz

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_timeDelay(7.,mMax[i],gamma[i],all_pzs,nTrials=3000,znorm=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(0.5,20,1000)
    p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*mergerRate[low_zs]/(mergerRate[0]*(1.+ref_zs))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))


#fig,ax = plt.subplots(figsize=(4,3))
#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity.pdf')
"""

# Get credible intervals
zs = np.arange(0.,3.05,0.05)
rates_5 = np.zeros(zs.size)
rates_25 = np.zeros(zs.size)
rates_50 = np.zeros(zs.size)
rates_75 = np.zeros(zs.size)
rates_95 = np.zeros(zs.size)
i_5 = int(np.round(0.05*localRates.size))
i_25 = int(np.round(0.25*localRates.size))
i_50 = int(np.round(0.50*localRates.size))
i_75 = int(np.round(0.75*localRates.size))
i_95 = int(np.round(0.95*localRates.size))
for i,z in enumerate(zs):

    rates_at_z = np.sort(np.array([\
        localRates[j]*np.power(1.+z,alpha[j])*(1.+np.power(1./(1.+zpeak[j]),alpha[j]+beta[j]))/(1.+np.power((1.+z)/(1.+zpeak[j]),alpha[j]+beta[j]))\
        for j in range(localRates.size)]))
    rates_5[i] = rates_at_z[i_5]
    rates_25[i] = rates_at_z[i_25]
    rates_50[i] = rates_at_z[i_50]
    rates_75[i] = rates_at_z[i_75]
    rates_95[i] = rates_at_z[i_95]

"""

fig,ax = plt.subplots(figsize=(4,3))
for i in range(gamma.size):   
    if localRates[i]!=0:

        # Convolve formation rate with time-delay distribution
        dpdt = np.power(tdelays,lmbda[i])
        dpdt[tdelays<0.05] = 0.
        dpdt[tdelays>13.5] = 0.
        mergerRate = formationRates.dot(dpdt)

        ax.plot(all_zs,localRates[i]*mergerRate/mergerRate[0],alpha=0.08,color='black',lw=0.5,zorder=0)

#ax.fill_between(zs,rates_5,rates_95,facecolor='#9ecae1',alpha=0.6)
#ax.fill_between(zs,rates_25,rates_75,facecolor='#2171b5',alpha=0.6)
#ax.plot(zs,rates_50,color='#08519c')

SFR_md = np.power(1.+all_zs,2.7)/(1.+np.power((1.+all_zs)/2.9,5.6))
plt.plot(all_zs,20.*SFR_md/SFR_md[0])

ax.set_yscale('log')
ax.set_xlim([0,3])
ax.set_ylim([1e-4,1e8])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate.pdf',bbox_inches='tight')
