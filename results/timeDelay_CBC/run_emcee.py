import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../../code/')
from loadSamples import loadSamples
from gwBackground import *
from detectionFraction import detectionFractionMaya_timeDelay
from detectionFraction import detectionFractionMaya
from timeDelay import timeDelay

Nobs = 10.
Tobs = 169.7/365.25
year = 365.25*24*3600.

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 7.
gamma_Min = -4.
gamma_Max = 12.
lmbda_Max = 4.
lmbda_Min = -4.
alpha = 2.7
beta = 5.6
zpeak = 1.9
zmax = 6.
N_Min = 10.**0.5
N_Max = 10.**30.

# BBH data
m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("./../../GWTC-1_sample_release/GW150914_GWTC-1.hdf5")
m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012 = loadSamples("./../../GWTC-1_sample_release/GW151012_GWTC-1.hdf5")
m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226 = loadSamples("./../../GWTC-1_sample_release/GW151226_GWTC-1.hdf5")
m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104 = loadSamples("./../../GWTC-1_sample_release/GW170104_GWTC-1.hdf5")
m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608 = loadSamples("./../../GWTC-1_sample_release/GW170608_GWTC-1.hdf5")
m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729 = loadSamples("./../../GWTC-1_sample_release/GW170729_GWTC-1.hdf5")
m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809 = loadSamples("./../../GWTC-1_sample_release/GW170809_GWTC-1.hdf5")
m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814 = loadSamples("./../../GWTC-1_sample_release/GW170814_GWTC-1.hdf5")
m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818 = loadSamples("./../../GWTC-1_sample_release/GW170818_GWTC-1.hdf5")
m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823 = loadSamples("./../../GWTC-1_sample_release/GW170823_GWTC-1.hdf5")

sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170729,z_samples_170729,priors_170729]),\
    6:np.array([m1_samples_170809,z_samples_170809,priors_170809]),\
    7:np.array([m1_samples_170814,z_samples_170814,priors_170814]),\
    8:np.array([m1_samples_170818,z_samples_170818,priors_170818]),\
    9:np.array([m1_samples_170823,z_samples_170823,priors_170823]),\
    }

# Import redshift data (all in units of Gpc) and construct probability distribution values
all_zs,ref_dcs,ref_dLs,all_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = all_zs<0.9
ref_zs = all_zs[low_zs]
ref_dVc_dz = all_dVc_dz[low_zs]

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('./../../code/delayedRateData.npy')[()]
formationRates = rateData['formationRates']
tdelays = rateData['tds']
zformation = rateData['zs']

def loglikelihood(c):

    # Read parameters
    gamma = c[0]
    mMax = c[1]
    lmbda = c[2]

    if gamma<gamma_Min or gamma>gamma_Max or mMax<mMax_Min or mMax>mMax_Max or lmbda<lmbda_Min or lmbda>lmbda_Max:
        return -np.inf

    else:
        logP = 0.

        # Convolve formation rate with time-delay distribution
        dpdt = np.power(tdelays,lmbda)
        dpdt[tdelays<0.05] = 0.
        dpdt[tdelays>13.5] = 0.
        mergerRate = formationRates.dot(dpdt)

        # Construct redshift probability distribution
        all_pzs = all_dVc_dz*mergerRate/(1.+all_zs)
        ref_pzs = ref_dVc_dz*mergerRate[low_zs]/(1.+ref_zs)
        dz = ref_zs[1] - ref_zs[0]
        ref_pzs /= np.sum(ref_pzs)*dz

        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]
            
            # Mass 1 probability
            if gamma==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-gamma)*np.power(m1_samples,-gamma)/(np.power(mMax,1.-gamma)-np.power(mMin,1.-gamma))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above mMax
            p_m1[m1_samples>mMax] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        # Construct CDF for total event counts
        Ntots = np.logspace(0.5,20,1000)
        dFraction = detectionFractionMaya_timeDelay(mMin,mMax,gamma,all_pzs,nTrials=3000,znorm=True)
        p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
        p_Ns /= np.sum(p_Ns)
        cdf_Ns = np.cumsum(p_Ns)

        # Get random rate and convert to local rate density
        c = np.random.random()
        totalRate = np.interp(c,cdf_Ns,Ntots)/Tobs
        pz_unnormed = ref_dVc_dz*mergerRate[low_zs]/(mergerRate[0]*(1.+ref_zs))
        pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
        R0 = totalRate/pz_integral/(4.*np.pi)

        print(mMax,gamma,lmbda,logP,R0)

        return logP - 10.*np.log(dFraction)

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 8
    initial_gammas = np.random.random(nWalkers)*(gamma_Max-gamma_Min)+gamma_Min
    initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    initial_lmbdas = np.random.random(nWalkers)*(lmbda_Max-lmbda_Min)+lmbda_Min
    initial_walkers = np.transpose([initial_gammas,initial_mMaxs,initial_lmbdas])

    # Run
    nSteps = 2000
    sampler = mc.EnsembleSampler(nWalkers,3,loglikelihood,threads=4)
    sampler.run_mcmc(initial_walkers,nSteps)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]
    print(chainBurned)

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(3):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*3)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print(chainDownsampled)
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples.npy',chainDownsampled)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

