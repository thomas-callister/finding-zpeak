import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import LinearNDInterpolator
import sys
sys.path.append('./../code/')
from loadSamples import loadSamples
from detectionFraction import detectionFractionMaya

# Prior bounds
#a_Min = -4.
#a_Max = 12.
alpha = 1.5
lmbda_Min = -25.
lmbda_Max = 25.
mMin = 7.
mMax = 40.

# BBH data
m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("GWTC-1_sample_release/GW150914_GWTC-1.hdf5")
m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012 = loadSamples("GWTC-1_sample_release/GW151012_GWTC-1.hdf5")
m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226 = loadSamples("GWTC-1_sample_release/GW151226_GWTC-1.hdf5")
m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104 = loadSamples("GWTC-1_sample_release/GW170104_GWTC-1.hdf5")
m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608 = loadSamples("GWTC-1_sample_release/GW170608_GWTC-1.hdf5")
m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729 = loadSamples("GWTC-1_sample_release/GW170729_GWTC-1.hdf5")
m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809 = loadSamples("GWTC-1_sample_release/GW170809_GWTC-1.hdf5")
m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814 = loadSamples("GWTC-1_sample_release/GW170814_GWTC-1.hdf5")
m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818 = loadSamples("GWTC-1_sample_release/GW170818_GWTC-1.hdf5")
m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823 = loadSamples("GWTC-1_sample_release/GW170823_GWTC-1.hdf5")

sampleDict = {\
    0:np.array([m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729]),\
    6:np.array([m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809]),\
    7:np.array([m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814]),\
    8:np.array([m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818]),\
    9:np.array([m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823]),\
    }
"""
sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170814,z_samples_170814,priors_170814]),\
    }
"""

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)

def loglikelihood(a,lmbda):

    # Construct redshift probability distribution
    ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,lmbda-1.)
    ref_pzs = ref_pzs/np.sum(ref_pzs)

    logP = 0.
    print("Lambda: ",lmbda)
    for event in sampleDict:

        m1_samples = sampleDict[event][0]
        m2_samples = sampleDict[event][1]
        z_samples = sampleDict[event][2]
        priors = sampleDict[event][3]
        
        # Mass 1 probability
        if a==1:
            p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
        else:
            p_m1 = (1.-a)*np.power(m1_samples,-a)/(np.power(mMax,1.-a)-np.power(mMin,1.-a))

        # Mass 2 probability
        p_m2 = 1./(m1_samples-mMin)

        # Zero out probabilities above mMax
        p_m1[m1_samples>mMax] = 0
        p_m1[m1_samples<mMin] = 0
        p_m2[m2_samples>mMax] = 0
        p_m2[m2_samples<mMin] = 0

        # Redshift probability
        p_z = np.interp(z_samples,ref_zs,ref_pzs)
        p_z[z_samples>0.9] = 0

        # Evaluate marginalized likelihood
        nSamples = m1_samples.size
        pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

        # Add
        logP += np.log(pEvidence)

    return logP - 10.*np.log(detectionFractionMaya(mMin,mMax,a,lmbda))

if __name__=="__main__":

    """
    a_vals = np.linspace(-4.,12.,20)
    lmbda_vals = np.linspace(-25.,25.,20)
    postProb = np.zeros((a_vals.size,lmbda_vals.size))
    for i in range(a_vals.size):
        for j in range(lmbda_vals.size):
            postProb[i,j] = loglikelihood(a_vals[i],lmbda_vals[j])
            print(i,j,a_vals[i],lmbda_vals[j],postProb[i,j])

    postProb = postProb - np.max(postProb)
    postProb = np.exp(postProb)
    postProb = postProb/np.sum(postProb)
    np.save('posterior_grid.npy',postProb)

    fig,ax = plt.subplots(figsize=(4,3))
    ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    ax.set_xlabel(r'$\alpha$',fontsize=10)
    ax.set_ylabel(r'$\lambda$',fontsize=10)
    ax.xaxis.grid(True,which='major',ls=':')
    ax.yaxis.grid(True,which='major',ls=':')
    ax.set_xlim([-4,12])
    ax.set_ylim(-25,25)
    plt.tight_layout()
    ax.tick_params(labelsize=8)
    plt.savefig("./redshiftFit_Grid_altPzNorm.pdf")
    """

    lmbda_vals = np.linspace(-30.,30.,50)
    postProb = np.zeros(lmbda_vals.size)
    for i,lmbda in enumerate(lmbda_vals):
        postProb[i] = loglikelihood(alpha,lmbda)

    postProb = postProb - np.max(postProb)
    postProb = np.exp(postProb)
    postProb = postProb/np.sum(postProb)

    fig,ax = plt.subplots(figsize=(4,3))
    ax.plot(lmbda_vals,postProb)
    ax.xaxis.grid(True,which='major')
    ax.yaxis.grid(True,which='major')
    plt.savefig('./lambdaPosterior.pdf')
