import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
import matplotlib.pyplot as plt
import numpy as np

data = np.load('samples.npy')

#data = data[:,400:,:]
#data = data.reshape((-1,len(data[0,0,:])))
#print(data.shape)

gamma = data[:,0]
alpha = data[:,1]
mMax = data[:,2]

print(gamma)

fig = plt.figure(figsize=(12,9))

ax_g = fig.add_subplot(331)
ax_g.hist(gamma,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g.set_axisbelow(True)
plt.setp(ax_g.get_xticklabels(), visible=False)
plt.setp(ax_g.get_yticklabels(), visible=False)

ax_gm = fig.add_subplot(334,sharex=ax_g)
ax_gm.hexbin(gamma,mMax,cmap='Blues',gridsize=15)
ax_gm.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.set_ylabel(r'$M_\mathrm{max}$',fontsize=16)
plt.setp(ax_gm.get_xticklabels(), visible=False)

ax_ga = fig.add_subplot(337,sharex=ax_g)
ax_ga.hexbin(gamma,alpha,cmap='Blues',gridsize=15)
ax_ga.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ga.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ga.set_xlabel(r'$\gamma$',fontsize=16)
ax_ga.set_ylabel(r'$\alpha$',fontsize=16)

ax_m = fig.add_subplot(335)
ax_m.hist(mMax,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_m.set_axisbelow(True)
plt.setp(ax_m.get_xticklabels(), visible=False)
plt.setp(ax_m.get_yticklabels(), visible=False)

ax_ma = fig.add_subplot(338,sharex=ax_m)
ax_ma.hexbin(mMax,alpha,cmap='Blues',gridsize=15)
ax_ma.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.set_xlabel(r'$M_\mathrm{max}$',fontsize=16)
plt.setp(ax_ma.get_yticklabels(), visible=False)

ax_a = fig.add_subplot(339)
ax_a.hist(alpha,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.set_axisbelow(True)
ax_a.set_xlabel(r'$\alpha$',fontsize=16)
plt.setp(ax_a.get_yticklabels(), visible=False)

plt.tight_layout()
plt.savefig('corner.pdf',bbox_inches='tight')


