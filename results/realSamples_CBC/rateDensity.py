import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya
from scipy.interpolate import LinearNDInterpolator
import matplotlib.style
matplotlib.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')
matplotlib.use('Agg')
import matplotlib.pyplot as plt

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.

# Load samples
data = np.load('samples_noRate.npy')
#data = data[:,400:,:]
#data = data.reshape((-1,len(data[0,0,:])))
alpha = data[:,0]
lmbda = data[:,1]
mMax = data[:,2]
#Ntot = 10.**data[:,3]

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<0.89
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

"""

totalRates = np.zeros(alpha.size)
localRates = np.zeros(alpha.size)
print(alpha.size)

# Loop across samples
for i in range(alpha.size):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya(mMin,mMax[i],alpha[i],lmbda[i],znorm=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(0.5,10,500)
    p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    # Rdensity(z) = Rdensity_0 (1+z)**lmbda
    # R = \int Rdensity(z) (dVc/dz)/(1+z) dz
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,lmbda[i]-1)
    pz_integral = np.sum(pz_unnormed[ref_zs<0.89])*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)

    print(localRates[i])
    print(np.mean(localRates[localRates!=0]),np.std(localRates[localRates!=0]))

#fig,ax = plt.subplots(figsize=(4,3))
#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity_auto.pdf')

#np.savetxt('localRates.dat',localRates)
#localRates = np.loadtxt('localRates.dat')
"""

#np.save('localRates.npy',localRates)
localRates = np.load('localRates.npy')

# Get credible intervals
zs = np.arange(0.,1.,0.01)
rates_2sigma_low = np.zeros(zs.size)
rates_2sigma_high = np.zeros(zs.size)
rates_1sigma_low = np.zeros(zs.size)
rates_1sigma_high = np.zeros(zs.size)
i_2sigma_low = int(np.round(0.025*localRates.size))
i_2sigma_high = int(np.round(0.975*localRates.size))
i_1sigma_low = int(np.round(0.26*localRates.size))
i_1sigma_high = int(np.round(0.84*localRates.size))

for i,z in enumerate(zs):
    print(z)
    rates_at_z = np.sort(np.array([localRates[j]*np.power(1.+z,lmbda[j]) for j in range(localRates.size)]))
    rates_5[i] = rates_at_z[i_5]
    rates_25[i] = rates_at_z[i_25]
    rates_50[i] = rates_at_z[i_50]
    rates_75[i] = rates_at_z[i_75]
    rates_95[i] = rates_at_z[i_95]

fig,ax = plt.subplots(figsize=(6,3))
for i in range(alpha.size):   
    ax.plot(zs,localRates[i]*np.power(1.+zs,lmbda[i]),alpha=0.1,color='black',lw=0.5)
    #ax.fill_between(zs,rates_5,rates_95,facecolor='#9ecae1',alpha=0.8)
    #ax.fill_between(zs,rates_25,rates_75,facecolor='#2171b5',alpha=0.8)
    #ax.plot(zs,rates_50,color='#08519c')
ax.set_yscale('log')
ax.set_xlim(0,5)
ax.set_ylim([1e-3,1e9])
#ax.set_xlim(0,1)
#ax.set_ylim([1e-2,1e7])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate_inPost_z5.pdf',bbox_inches='tight')
