import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../code/')
from loadSamples import loadSamples
from detectionFraction import detectionFractionMaya
from scipy.interpolate import LinearNDInterpolator

# Prior bounds
a_Min = -4.
a_Max = 12.
lmbda_Min = -25.
lmbda_Max = 25.
mMax_Min = 30.
mMax_Max = 100.
mMin = 7.
#mMax = 39.

# BBH data
m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("GWTC-1_sample_release/GW150914_GWTC-1.hdf5")
m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012 = loadSamples("GWTC-1_sample_release/GW151012_GWTC-1.hdf5")
m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226 = loadSamples("GWTC-1_sample_release/GW151226_GWTC-1.hdf5")
m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104 = loadSamples("GWTC-1_sample_release/GW170104_GWTC-1.hdf5")
m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608 = loadSamples("GWTC-1_sample_release/GW170608_GWTC-1.hdf5")
m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729 = loadSamples("GWTC-1_sample_release/GW170729_GWTC-1.hdf5")
m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809 = loadSamples("GWTC-1_sample_release/GW170809_GWTC-1.hdf5")
m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814 = loadSamples("GWTC-1_sample_release/GW170814_GWTC-1.hdf5")
m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818 = loadSamples("GWTC-1_sample_release/GW170818_GWTC-1.hdf5")
m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823 = loadSamples("GWTC-1_sample_release/GW170823_GWTC-1.hdf5")

sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170729,z_samples_170729,priors_170729]),\
    6:np.array([m1_samples_170809,z_samples_170809,priors_170809]),\
    7:np.array([m1_samples_170814,z_samples_170814,priors_170814]),\
    8:np.array([m1_samples_170818,z_samples_170818,priors_170818]),\
    9:np.array([m1_samples_170823,z_samples_170823,priors_170823]),\
    }

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)

resultsDict = np.load('./../code/detectionFractionData.npy')[()]
coords = resultsDict['coords']
logfracs = resultsDict['logfracs']
logDetectionFraction = LinearNDInterpolator(coords,logfracs,rescale=True)

def loglikelihood(c):

    # Read parameters
    a = c[0]
    lmbda = c[1]
    mMax = c[2]

    if a<a_Min or a>a_Max or lmbda<lmbda_Min or lmbda>lmbda_Max or mMax<mMax_Min or mMax>mMax_Max:
        return -np.inf

    else:

        # Construct redshift probability distribution
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,lmbda-1.)
        ref_pzs = ref_pzs/np.sum(ref_pzs)

        logP = 0.
        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]
            
            # Mass 1 probability
            if a==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-a)*np.power(m1_samples,-a)/(np.power(mMax,1.-a)-np.power(mMin,1.-a))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above mMax
            p_m1[m1_samples>mMax] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        print(a,lmbda,mMax,logP)

        return logP - 10.*logDetectionFraction(a,lmbda,mMax)

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 10
    initial_as = np.random.random(nWalkers)*(a_Max-a_Min)+a_Min
    initial_lmbdas = np.random.random(nWalkers)*(lmbda_Max-lmbda_Min)+lmbda_Min
    initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    initial_walkers = np.transpose([initial_as,initial_lmbdas,initial_mMaxs])
    #initial_walkers = np.transpose([initial_as,initial_lmbdas])

    # Run
    nSteps = 500
    sampler = mc.EnsembleSampler(nWalkers,3,loglikelihood)
    sampler.run_mcmc(initial_walkers,nSteps)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(3):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*3)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    #np.save('samples_test.npy',chainDownsampled)
    np.save('samples_noRate.npy',sampler.chain)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

