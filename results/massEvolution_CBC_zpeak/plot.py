import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
import matplotlib.pyplot as plt
import numpy as np

data = np.load('samples_1.npy')
gamma1 = data[:,0]
gamma2 = data[:,1]
mMax = data[:,2]
alpha = data[:,3]
beta = data[:,4]
zpeak = data[:,5]

"""
data2 = np.load('samples_0.npy')
gamma1 = np.append(gamma1,data2[:,0])
gamma2 = np.append(gamma2,data2[:,1])
mMax = np.append(mMax,data2[:,2])
alpha = np.append(alpha,data2[:,3])
beta = np.append(beta,data2[:,4])
zpeak = np.append(zpeak,data2[:,5])
"""

print(np.max(alpha))

fig = plt.figure(figsize=(12,9))

#############
# gamma1
#############

ax_g1 = fig.add_subplot(661)
ax_g1.hist(gamma1,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g1.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1.set_axisbelow(True)
plt.setp(ax_g1.get_xticklabels(), visible=False)

ax_g1g2 = fig.add_subplot(667,sharex=ax_g1)
ax_g1g2.hexbin(gamma1,gamma2,cmap='Blues',gridsize=15)
ax_g1g2.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1g2.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1g2.set_ylabel(r'$\gamma_2$',fontsize=16)
plt.setp(ax_g1g2.get_xticklabels(), visible=False)

ax_g1m = fig.add_subplot(6,6,13,sharex=ax_g1)
ax_g1m.hexbin(gamma1,mMax,cmap='Blues',gridsize=15)
ax_g1m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1m.set_ylabel(r'$M_\mathrm{Max}$',fontsize=16)
plt.setp(ax_g1m.get_xticklabels(), visible=False)

ax_g1a = fig.add_subplot(6,6,19,sharex=ax_g1)
ax_g1a.hexbin(gamma1,alpha,cmap='Blues',gridsize=15)
ax_g1a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1a.set_ylabel(r'$\alpha$',fontsize=16)
plt.setp(ax_g1a.get_xticklabels(), visible=False)

ax_g1b = fig.add_subplot(6,6,25,sharex=ax_g1)
ax_g1b.hexbin(gamma1,beta,cmap='Blues',gridsize=15)
ax_g1b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1b.set_ylabel(r'$\beta$',fontsize=16)
plt.setp(ax_g1b.get_xticklabels(), visible=False)

ax_g1z = fig.add_subplot(6,6,31,sharex=ax_g1)
ax_g1z.hexbin(gamma1,zpeak,cmap='Blues',gridsize=15)
ax_g1z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g1z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g1z.set_xlabel(r'$\gamma_1$',fontsize=16)
ax_g1z.set_ylabel(r'$z_\mathrm{peak}$',fontsize=16)

##############
# gamma2
##############

ax_g2 = fig.add_subplot(6,6,8)
ax_g2.hist(gamma2,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g2.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g2.set_axisbelow(True)
plt.setp(ax_g2.get_xticklabels(), visible=False)
plt.setp(ax_g2.get_yticklabels(), visible=False)

ax_g2m = fig.add_subplot(6,6,14,sharex=ax_g2)
ax_g2m.hexbin(gamma2,mMax,cmap='Blues',gridsize=15)
ax_g2m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2m.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_g2m.get_xticklabels(), visible=False)
plt.setp(ax_g2m.get_yticklabels(), visible=False)

ax_g2a = fig.add_subplot(6,6,20,sharex=ax_g2)
ax_g2a.hexbin(gamma2,alpha,cmap='Blues',gridsize=15)
ax_g2a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2a.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_g2a.get_xticklabels(), visible=False)
plt.setp(ax_g2a.get_yticklabels(), visible=False)

ax_g2b = fig.add_subplot(6,6,26,sharex=ax_g2)
ax_g2b.hexbin(gamma2,beta,cmap='Blues',gridsize=15)
ax_g2b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2b.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_g2b.get_xticklabels(), visible=False)
plt.setp(ax_g2b.get_yticklabels(), visible=False)

ax_g2z = fig.add_subplot(6,6,32,sharex=ax_g2)
ax_g2z.hexbin(gamma2,zpeak,cmap='Blues',gridsize=15)
ax_g2z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g2z.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_g2z.get_yticklabels(), visible=False)
ax_g2z.set_xlabel(r'$\gamma_2$',fontsize=16)

#############
# Mmax
#############

ax_m = fig.add_subplot(6,6,15)
ax_m.hist(mMax,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_m.set_axisbelow(True)
plt.setp(ax_m.get_xticklabels(), visible=False)
plt.setp(ax_m.get_yticklabels(), visible=False)

ax_ma = fig.add_subplot(6,6,21,sharex=ax_m)
ax_ma.hexbin(mMax,alpha,cmap='Blues',gridsize=15)
ax_ma.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_ma.get_xticklabels(), visible=False)
plt.setp(ax_ma.get_yticklabels(), visible=False)

ax_mb = fig.add_subplot(6,6,27,sharex=ax_m)
ax_mb.hexbin(mMax,beta,cmap='Blues',gridsize=15)
ax_mb.xaxis.grid(True,which='major',ls=':',color='grey')
ax_mb.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_mb.get_xticklabels(), visible=False)
plt.setp(ax_mb.get_yticklabels(), visible=False)

ax_mz = fig.add_subplot(6,6,33,sharex=ax_m)
ax_mz.hexbin(mMax,zpeak,cmap='Blues',gridsize=15)
ax_mz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_mz.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_mz.get_yticklabels(), visible=False)
ax_mz.set_xlabel(r'$M_\mathrm{Max}$',fontsize=16)

################
# alpha
################

ax_a = fig.add_subplot(6,6,22)
ax_a.hist(alpha,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_xticklabels(), visible=False)
plt.setp(ax_a.get_yticklabels(), visible=False)

ax_ab = fig.add_subplot(6,6,28,sharex=ax_a)
ax_ab.hexbin(alpha,beta,cmap='Blues',gridsize=15)
ax_ab.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_ab.get_xticklabels(), visible=False)
plt.setp(ax_ab.get_yticklabels(), visible=False)

ax_az = fig.add_subplot(6,6,34,sharex=ax_a)
ax_az.hexbin(alpha,beta,cmap='Blues',gridsize=15)
ax_az.xaxis.grid(True,which='major',ls=':',color='grey')
ax_az.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_az.get_yticklabels(), visible=False)
ax_az.set_xlabel(r'$\alpha$',fontsize=16)

###########
# beta
###########

ax_b = fig.add_subplot(6,6,29)
ax_b.hist(beta,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_b.set_axisbelow(True)
plt.setp(ax_b.get_xticklabels(), visible=False)
plt.setp(ax_b.get_yticklabels(), visible=False)

ax_bz = fig.add_subplot(6,6,35,sharex=ax_b)
ax_bz.hexbin(beta,zpeak,cmap='Blues',gridsize=15)
ax_bz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.yaxis.grid(True,which='major',ls=':',color='grey')
plt.setp(ax_bz.get_yticklabels(), visible=False)
ax_bz.set_xlabel(r'$\beta$',fontsize=16)

########
# zpeak
########

ax_z = fig.add_subplot(6,6,36)
ax_z.hist(zpeak,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_z.set_axisbelow(True)
plt.setp(ax_z.get_yticklabels(), visible=False)
ax_z.set_xlabel(r'$z_\mathrm{peak}$',fontsize=16)



plt.tight_layout()
plt.savefig('corner.pdf',bbox_inches='tight')


