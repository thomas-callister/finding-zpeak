import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../../code/')
from loadSamples import loadSamples
from detectionFraction import detectionFractionMaya_massEvolution_extended

# Prior bounds
gamma1_Min = -4.
gamma1_Max = 12.
gamma2_Min = 0.
gamma2_Max = 5.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.
mMax_Min = 7.1
mMax_Max = 100.
mMin = 7.
#mMax = 39.

# BBH data
m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("./../../GWTC-1_sample_release/GW150914_GWTC-1.hdf5")
m1_samples_151012,m2_samples_151012,z_samples_151012,priors_151012 = loadSamples("./../../GWTC-1_sample_release/GW151012_GWTC-1.hdf5")
m1_samples_151226,m2_samples_151226,z_samples_151226,priors_151226 = loadSamples("./../../GWTC-1_sample_release/GW151226_GWTC-1.hdf5")
m1_samples_170104,m2_samples_170104,z_samples_170104,priors_170104 = loadSamples("./../../GWTC-1_sample_release/GW170104_GWTC-1.hdf5")
m1_samples_170608,m2_samples_170608,z_samples_170608,priors_170608 = loadSamples("./../../GWTC-1_sample_release/GW170608_GWTC-1.hdf5")
m1_samples_170729,m2_samples_170729,z_samples_170729,priors_170729 = loadSamples("./../../GWTC-1_sample_release/GW170729_GWTC-1.hdf5")
m1_samples_170809,m2_samples_170809,z_samples_170809,priors_170809 = loadSamples("./../../GWTC-1_sample_release/GW170809_GWTC-1.hdf5")
m1_samples_170814,m2_samples_170814,z_samples_170814,priors_170814 = loadSamples("./../../GWTC-1_sample_release/GW170814_GWTC-1.hdf5")
m1_samples_170818,m2_samples_170818,z_samples_170818,priors_170818 = loadSamples("./../../GWTC-1_sample_release/GW170818_GWTC-1.hdf5")
m1_samples_170823,m2_samples_170823,z_samples_170823,priors_170823 = loadSamples("./../../GWTC-1_sample_release/GW170823_GWTC-1.hdf5")

sampleDict = {\
    0:np.array([m1_samples_150914,z_samples_150914,priors_150914]),\
    1:np.array([m1_samples_151012,z_samples_151012,priors_151012]),\
    2:np.array([m1_samples_151226,z_samples_151226,priors_151226]),\
    3:np.array([m1_samples_170104,z_samples_170104,priors_170104]),\
    4:np.array([m1_samples_170608,z_samples_170608,priors_170608]),\
    5:np.array([m1_samples_170729,z_samples_170729,priors_170729]),\
    6:np.array([m1_samples_170809,z_samples_170809,priors_170809]),\
    7:np.array([m1_samples_170814,z_samples_170814,priors_170814]),\
    8:np.array([m1_samples_170818,z_samples_170818,priors_170818]),\
    9:np.array([m1_samples_170823,z_samples_170823,priors_170823]),\
    }

# Import redshift data (all in units of Gpc) and construct probability distribution values
all_zs,all_dcs,all_dLs,all_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
dz = all_zs[1] - all_zs[0]
low_zs = all_zs<0.9
ref_zs = all_zs[low_zs]
ref_dVc_dz = all_dVc_dz[low_zs]

def loglikelihood(c):

    # Read parameters
    gamma1 = c[0]
    gamma2 = c[1]
    mMax = c[2]
    alpha = c[3]
    beta = c[4]
    zpeak = c[5]

    if gamma1<gamma1_Min or gamma1>gamma1_Max or gamma2<gamma2_Min or gamma2>gamma2_Max or alpha<alpha_Min or alpha>alpha_Max or beta<beta_Min or beta>beta_Max or zpeak<zpeak_Min or zpeak>zpeak_Max or mMax<mMax_Min or mMax>mMax_Max:
        return -np.inf

    else:

        # Construct redshift probability distribution
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,alpha-1.)/(1.+np.power((1.+ref_zs)/(1.+zpeak),alpha+beta))
        ref_pzs = ref_pzs/np.sum(ref_pzs*dz)

        logP = 0.
        for event in sampleDict:

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]
            
            # Mass 1 probability
            mMax_z = mMax*np.power(1.+z_samples,gamma2)
            if gamma1==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax_z/mMin)
            else:
                p_m1 = (1.-gamma1)*np.power(m1_samples,-gamma1)/(np.power(mMax_z,1.-gamma1)-np.power(mMin,1.-gamma1))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above (redshift dependent) mMax
            p_m1[m1_samples>mMax_z] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        print(gamma1,gamma2,alpha,beta,zpeak,mMax,logP)
        return logP - 10.*np.log(detectionFractionMaya_massEvolution_extended(mMin,mMax,gamma1,gamma2,alpha,beta,zpeak,nTrials=3000,znorm=True))

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 16
    initial_gamma1s = np.random.random(nWalkers)*(gamma1_Max-gamma1_Min)+gamma1_Min
    initial_gamma2s = np.random.random(nWalkers)*(gamma2_Max-gamma2_Min)+gamma2_Min
    initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    initial_alphas = np.random.random(nWalkers)*(alpha_Max-alpha_Min)+alpha_Min
    initial_betas = np.random.random(nWalkers)*(beta_Max-beta_Min)+beta_Min
    initial_zpeaks = np.random.random(nWalkers)*(zpeak_Max-zpeak_Min)+zpeak_Min
    initial_walkers = np.transpose([initial_gamma1s,initial_gamma2s,initial_mMaxs,initial_alphas,initial_betas,initial_zpeaks])

    # Run
    nSteps = 3000
    print("THREADS")
    sampler = mc.EnsembleSampler(nWalkers,6,loglikelihood,threads=4)
    sampler.run_mcmc(initial_walkers,nSteps)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(6):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*6)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples.npy',chainDownsampled)


    #fig,ax = plt.subplots()
    #ax.pcolormesh(a_vals,lmbda_vals,postProb.T,cmap='Blues')
    #plt.savefig("/home/thomas.callister/public_html/Scratch/redshiftFit.pdf")

