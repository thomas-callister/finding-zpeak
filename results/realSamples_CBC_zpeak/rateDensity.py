import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_extended
from scipy.interpolate import LinearNDInterpolator
from matplotlib import rc
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.

# Load samples
data = np.load('processed_raw_samples_wRate.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
localRates = data[:,5]

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<1.5
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

"""
totalRates = np.zeros(alpha.size)
localRates = np.zeros(alpha.size)
print(alpha.size)

# Loop across samples
for i in range(alpha.size):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_extended(7.,mMax[i],gamma[i],alpha[i],beta[i],zpeak[i],nTrials=3000,znorm=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(0.5,20,1000)
    p_Ns = np.power(Ntots*dFraction,Nobs)*np.exp(-Ntots*dFraction)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha[i]-1)*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+ref_zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))

np.save('localRates.npy',localRates)
localRates = np.load('localRates.npy')
"""

#fig,ax = plt.subplots(figsize=(4,3))
#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity.pdf')

# Get credible intervals
zs = np.arange(0.,5.05,0.05)
rateArray = np.zeros((localRates.size,zs.size))
for i in range(localRates.size):
    rateArray[i,:] = localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
z_2sigma_low = np.quantile(rateArray,0.025,axis=0)
z_1sigma_low = np.quantile(rateArray,0.26,axis=0)
z_1sigma_high = np.quantile(rateArray,0.84,axis=0)
z_2sigma_high = np.quantile(rateArray,0.975,axis=0)

fig,ax = plt.subplots(figsize=(5,3))
for i in range(alpha.size):   
    if localRates[i]!=0:
        #ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.1,color='#08519c',lw=0.5,zorder=0)
        ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.2,color='#4daf4a',lw=0.4,zorder=0)
#ax.fill_between(zs,rates_5,rates_95,facecolor='#9ecae1',alpha=0.6)
#ax.fill_between(zs,rates_25,rates_75,facecolor='#2171b5',alpha=0.6)
#ax.plot(zs,rates_50,color='#08519c')
ax.plot(zs,z_2sigma_low,color='#525252',lw=1.,alpha=0.8)
ax.plot(zs,z_2sigma_high,color='#525252',lw=1.,alpha=0.8)
ax.plot(zs,z_1sigma_low,color='#525252',ls='--',lw=1.,alpha=0.8)
ax.plot(zs,z_1sigma_high,color='#525252',ls='--',lw=1.,alpha=0.8)
ax.set_yscale('log')
ax.set_xlim([0,5])
ax.set_ylim([1e-4,1e8])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.tight_layout()
plt.savefig('mergerRate.pdf',bbox_inches='tight')
