import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

data = np.load('samples.npy')

gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
print(zpeak.size)

fig = plt.figure(figsize=(20,15))

ax_g = fig.add_subplot(551)
ax_g.hist(gamma,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_g.xaxis.grid(True,which='major',ls=':',color='grey')
ax_g.yaxis.grid(True,which='major',ls=':',color='grey')
ax_g.tick_params(axis='both', which='major', labelsize=14)
ax_g.set_axisbelow(True)
plt.setp(ax_g.get_xticklabels(), visible=False)

ax_gm = fig.add_subplot(556,sharex=ax_g)
ax_gm.hexbin(gamma,mMax,cmap='Blues',gridsize=15)
ax_gm.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gm.tick_params(axis='both', which='major', labelsize=14)
ax_gm.set_ylabel(r'$M_\mathrm{Max}$',fontsize=20)
plt.setp(ax_gm.get_xticklabels(), visible=False)

ax_ga = fig.add_subplot(5,5,11,sharex=ax_g)
ax_ga.hexbin(gamma,alpha,cmap='Blues',gridsize=15)
ax_ga.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ga.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ga.tick_params(axis='both', which='major', labelsize=14)
ax_ga.set_ylabel(r'$\alpha$',fontsize=20)
plt.setp(ax_ga.get_xticklabels(), visible=False)

ax_gb = fig.add_subplot(5,5,16,sharex=ax_g)
ax_gb.hexbin(gamma,beta,cmap='Blues',gridsize=15)
ax_gb.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gb.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gb.tick_params(axis='both', which='major', labelsize=14)
ax_gb.set_ylabel(r'$\beta$',fontsize=20)
plt.setp(ax_gb.get_xticklabels(), visible=False)

ax_gz = fig.add_subplot(5,5,21,sharex=ax_g)
ax_gz.hexbin(gamma,zpeak,cmap='Blues',gridsize=15)
ax_gz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_gz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_gz.tick_params(axis='both', which='major', labelsize=14)
ax_gz.set_xlabel(r'$\gamma$',fontsize=20)
ax_gz.set_ylabel(r'$z_\mathrm{peak}$',fontsize=20)

ax_m = fig.add_subplot(5,5,7)
ax_m.hist(mMax,bins=15,color='#e41a1c',normed=True,histtype='stepfilled',edgecolor='black')
ax_m.xaxis.grid(True,which='major',ls=':',color='grey')
ax_m.yaxis.grid(True,which='major',ls=':',color='grey')
ax_m.tick_params(axis='both', which='major', labelsize=14)
ax_m.set_axisbelow(True)
plt.setp(ax_m.get_xticklabels(), visible=False)
plt.setp(ax_m.get_yticklabels(), visible=False)

ax_ma = fig.add_subplot(5,5,12,sharex=ax_m)
ax_ma.hexbin(mMax,alpha,cmap='Blues',gridsize=15)
ax_ma.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ma.tick_params(axis='both', which='major', labelsize=14)
plt.setp(ax_ma.get_xticklabels(), visible=False)
plt.setp(ax_ma.get_yticklabels(), visible=False)

ax_mb = fig.add_subplot(5,5,17,sharex=ax_m)
ax_mb.hexbin(mMax,beta,cmap='Blues',gridsize=15)
ax_mb.xaxis.grid(True,which='major',ls=':',color='grey')
ax_mb.yaxis.grid(True,which='major',ls=':',color='grey')
ax_mb.tick_params(axis='both', which='major', labelsize=14)
plt.setp(ax_mb.get_xticklabels(), visible=False)
plt.setp(ax_mb.get_yticklabels(), visible=False)

ax_mz = fig.add_subplot(5,5,22,sharex=ax_m)
ax_mz.hexbin(mMax,zpeak,cmap='Blues',gridsize=15)
ax_mz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_mz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_mz.tick_params(axis='both', which='major', labelsize=14)
ax_mz.set_xlabel(r'$M_\mathrm{Max}$',fontsize=20)
plt.setp(ax_mz.get_yticklabels(), visible=False)

ax_a = fig.add_subplot(5,5,13)
ax_a.hist(alpha,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.tick_params(axis='both', which='major', labelsize=14)
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_xticklabels(), visible=False)
plt.setp(ax_a.get_yticklabels(), visible=False)

ax_ab = fig.add_subplot(5,5,18,sharex=ax_a)
ax_ab.hexbin(alpha,beta,cmap='Blues',gridsize=15)
ax_ab.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.tick_params(axis='both', which='major', labelsize=14)
plt.setp(ax_ab.get_xticklabels(), visible=False)
plt.setp(ax_ab.get_yticklabels(), visible=False)

ax_az = fig.add_subplot(5,5,23,sharex=ax_a)
ax_az.hexbin(alpha,zpeak,cmap='Blues',gridsize=15)
ax_az.xaxis.grid(True,which='major',ls=':',color='grey')
ax_az.yaxis.grid(True,which='major',ls=':',color='grey')
ax_az.tick_params(axis='both', which='major', labelsize=14)
ax_az.set_xlabel(r'$\alpha$',fontsize=20)
plt.setp(ax_az.get_yticklabels(), visible=False)

ax_b = fig.add_subplot(5,5,19)
ax_b.hist(beta,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_b.tick_params(axis='both', which='major', labelsize=14)
ax_b.set_axisbelow(True)
plt.setp(ax_b.get_xticklabels(), visible=False)
plt.setp(ax_b.get_yticklabels(), visible=False)

ax_bz = fig.add_subplot(5,5,24,sharex=ax_b)
ax_bz.hexbin(beta,zpeak,cmap='Blues',gridsize=15)
ax_bz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.tick_params(axis='both', which='major', labelsize=14)
ax_bz.set_xlabel(r'$\beta$',fontsize=20)
plt.setp(ax_bz.get_yticklabels(), visible=False)

ax_z = fig.add_subplot(5,5,25)
ax_z.hist(zpeak,bins=15,color='#4daf4a',normed=True,histtype='stepfilled',edgecolor='black')
ax_z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_z.tick_params(axis='both', which='major', labelsize=14)
ax_z.set_axisbelow(True)
ax_z.set_xlabel(r'$z_\mathrm{peak}$',fontsize=20)
plt.setp(ax_z.get_yticklabels(), visible=False)

plt.tight_layout()
plt.savefig('corner.pdf',bbox_inches='tight')


