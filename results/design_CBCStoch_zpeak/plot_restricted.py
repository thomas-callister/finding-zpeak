import matplotlib.style
import matplotlib as mpl
mpl.style.use('classic')
from matplotlib import rc
import matplotlib.pyplot as plt
import numpy as np

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

data = np.load('500events_samples.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]
print(zpeak.size)

dataCBC1 = np.load('./../design_CBC_zpeak/500events_samples_run1.npy')
dataCBC2 = np.load('./../design_CBC_zpeak/500events_samples_run2.npy')
dataCBC3 = np.load('./../design_CBC_zpeak/500events_samples_run3.npy')
gammaCBC = np.concatenate((dataCBC1[:,0],dataCBC2[:,0],dataCBC3[:,0]))[::1]
mMaxCBC = np.concatenate((dataCBC1[:,1],dataCBC2[:,1],dataCBC3[:,1]))[::1]
alphaCBC = np.concatenate((dataCBC1[:,2],dataCBC2[:,2],dataCBC3[:,2]))[::1]
betaCBC = np.concatenate((dataCBC1[:,3],dataCBC2[:,3],dataCBC3[:,3]))[::1]
zpeakCBC = np.concatenate((dataCBC1[:,4],dataCBC2[:,4],dataCBC3[:,4]))[::1]

print(zpeakCBC.size)


alphaMin = 1.5
alphaMax = 4.5
betaMin = 0
betaMax = 10
zpeakMin = 0
zpeakMax = 4

#red='#fb6a4a'
#blue='#1f78b4'
blue='#2171b5'
green='#4daf4a'


alpha = np.sort(alpha)
nsamps = alpha.size

med = alpha[int(nsamps/2.)]
low = alpha[int(nsamps*(0.5-0.34))]
high = alpha[int(nsamps*(0.5+0.34))]

print("med:\t{0}".format(med))
print("low:\t{0}".format(med-low))
print("high:\t{0}".format(high-med))

fig = plt.figure(figsize=(8,6))

ax_a = fig.add_subplot(3,3,1)
ax_a.hist(alpha,bins=np.linspace(alphaMin,alphaMax,12),color=blue,normed=True,histtype='stepfilled',edgecolor='black',zorder=1,alpha=0.9)
ax_a.hist(alphaCBC,bins=np.linspace(alphaMin,alphaMax,12),color=green,histtype='stepfilled',normed=True,edgecolor='black',zorder=-1)
ax_a.xaxis.grid(True,which='major',ls=':',color='grey')
ax_a.yaxis.grid(True,which='major',ls=':',color='grey')
ax_a.tick_params(axis='both', which='major', labelsize=10)
ax_a.set_axisbelow(True)
plt.setp(ax_a.get_xticklabels(), visible=False)
plt.setp(ax_a.get_yticklabels(), visible=False)

ax_ab = fig.add_subplot(3,3,4,sharex=ax_a)
ax_ab.hexbin(alpha,beta,cmap='Blues',gridsize=15,extent=(alphaMin,alphaMax,betaMin,betaMax))
ax_ab.xaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.yaxis.grid(True,which='major',ls=':',color='grey')
ax_ab.tick_params(axis='both', which='major', labelsize=10)
ax_ab.set_ylabel(r'$\beta$',fontsize=12)
plt.setp(ax_ab.get_xticklabels(), visible=False)

ax_az = fig.add_subplot(3,3,7,sharex=ax_a)
ax_az.hexbin(alpha,zpeak,cmap='Blues',gridsize=15,extent=(alphaMin,alphaMax,zpeakMin,zpeakMax))
ax_az.xaxis.grid(True,which='major',ls=':',color='grey')
ax_az.yaxis.grid(True,which='major',ls=':',color='grey')
ax_az.tick_params(axis='both', which='major', labelsize=10)
ax_az.set_xlabel(r'$\alpha$',fontsize=12)
ax_az.set_ylabel(r'$z_\mathrm{peak}$',fontsize=12)

ax_b = fig.add_subplot(3,3,5)
ax_b.hist(beta,bins=np.linspace(betaMin,betaMax,12),color=blue,normed=True,histtype='stepfilled',edgecolor='black',zorder=1,alpha=0.9)
ax_b.hist(betaCBC,bins=np.linspace(betaMin,betaMax,12),color=green,normed=True,histtype='stepfilled',edgecolor='black',zorder=-1)
ax_b.xaxis.grid(True,which='major',ls=':',color='grey')
ax_b.yaxis.grid(True,which='major',ls=':',color='grey')
ax_b.tick_params(axis='both', which='major', labelsize=10)
ax_b.set_axisbelow(True)
plt.setp(ax_b.get_xticklabels(), visible=False)
plt.setp(ax_b.get_yticklabels(), visible=False)

ax_bz = fig.add_subplot(3,3,8,sharex=ax_b)
ax_bz.hexbin(beta,zpeak,cmap='Blues',gridsize=15,extent=(betaMin,betaMax,zpeakMin,zpeakMax))
ax_bz.xaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.yaxis.grid(True,which='major',ls=':',color='grey')
ax_bz.tick_params(axis='both', which='major', labelsize=10)
ax_bz.set_xlabel(r'$\beta$',fontsize=12)
plt.setp(ax_bz.get_yticklabels(), visible=False)

ax_z = fig.add_subplot(3,3,9)
ax_z.hist(zpeak,bins=np.linspace(zpeakMin,zpeakMax,12),color=blue,normed=True,histtype='stepfilled',edgecolor='black',alpha=0.9)
ax_z.hist(zpeakCBC,bins=np.linspace(zpeakMin,zpeakMax,12),color=green,normed=True,histtype='stepfilled',edgecolor='black',zorder=-1)
ax_z.xaxis.grid(True,which='major',ls=':',color='grey')
ax_z.yaxis.grid(True,which='major',ls=':',color='grey')
ax_z.tick_params(axis='both', which='major', labelsize=10)
ax_z.set_axisbelow(True)
ax_z.set_xlabel(r'$z_\mathrm{peak}$',fontsize=12)
plt.setp(ax_z.get_yticklabels(), visible=False)

plt.tight_layout()
plt.savefig('corner_500events_restricted.pdf',bbox_inches='tight')


