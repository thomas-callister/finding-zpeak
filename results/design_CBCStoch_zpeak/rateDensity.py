import numpy as np
import os
import sys
sys.path.append('./../../code/')
from detectionFraction import detectionFractionMaya_extended
from scipy.interpolate import LinearNDInterpolator
from matplotlib import rc
import matplotlib as mpl
mpl.use("Agg")
import matplotlib.pyplot as plt
from computeTotalTime import obsTime
from loadMockSamples import *

rc('font', **{'family': 'serif', 'serif': ['Computer Modern']})
rc('text', usetex=True)

mMin = 5.

# Compute observation time
#sampleDict = loadMockSamples('./../../mock_detections/mockevents_design.h5')
#Nobs = len(sampleDict)
#Tobs = obsTime(Nobs,30.,1.2,45.,3.,3.,2.)

# Load samples
data = np.load('500events_samples.npy')
gamma = data[:,0]
mMax = data[:,1]
alpha = data[:,2]
beta = data[:,3]
zpeak = data[:,4]

"""
data1 = np.load('500events_samples_run1.npy')
data2 = np.load('500events_samples_run2.npy')
data3 = np.load('500events_samples_run3.npy')
gamma = np.concatenate((data1[:,0],data2[:,0],data3[:,0]))
mMax = np.concatenate((data1[:,1],data2[:,1],data3[:,1]))
alpha = np.concatenate((data1[:,2],data2[:,2],data3[:,2]))
beta = np.concatenate((data1[:,3],data2[:,3],data3[:,3]))
zpeak = np.concatenate((data1[:,4],data2[:,4],data3[:,4]))
"""

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<3.
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

"""
totalRates = np.zeros(alpha.size)
localRates = np.zeros(alpha.size)
print(alpha.size)

# Loop across samples
for i in range(alpha.size):
    print(i)

    # Get interpolated detection fraction and mean number of detections
    dFraction = detectionFractionMaya_extended(5.,mMax[i],gamma[i],alpha[i],beta[i],zpeak[i],nTrials=3000,znorm=True,design=True)

    # Construct posterior and cumulative density functions
    Ntots = np.logspace(3,8,1000)
    p_Ns_tmp = dFraction*Ntots*np.exp(-dFraction*Ntots/Nobs)
    p_Ns = np.power(p_Ns_tmp/np.max(p_Ns_tmp),Nobs)
    p_Ns /= np.sum(p_Ns)
    cdf_Ns = np.cumsum(p_Ns)

    # Get random rate
    c = np.random.random()
    totalRates[i] = np.interp(c,cdf_Ns,Ntots)/Tobs

    #totalRates[i] = Ntot[i]/Tobs

    # Get normalization constant
    pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha[i]-1)*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+ref_zs)/(1.+zpeak[i]),alpha[i]+beta[i]))
    pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
    localRates[i] = totalRates[i]/pz_integral/(4.*np.pi)
    print(localRates[i])

print(np.median(localRates))
print(np.max(localRates))
print(np.min(localRates))

np.save('localRates_500events.npy',localRates)
"""
localRates = np.load('localRates_500events.npy')

#fig,ax = plt.subplots(figsize=(4,3))
#ax.hist(np.log10(localRates),bins=20)
#plt.savefig('localRateDensity.pdf')

# Get credible intervals
zs = np.arange(0.,5.05,0.05)
"""
rates_5 = np.zeros(zs.size)
rates_25 = np.zeros(zs.size)
rates_50 = np.zeros(zs.size)
rates_75 = np.zeros(zs.size)
rates_95 = np.zeros(zs.size)
i_5 = int(np.round(0.05*localRates.size))
i_25 = int(np.round(0.25*localRates.size))
i_50 = int(np.round(0.50*localRates.size))
i_75 = int(np.round(0.75*localRates.size))
i_95 = int(np.round(0.95*localRates.size))
for i,z in enumerate(zs):

    rates_at_z = np.sort(np.array([\
        localRates[j]*np.power(1.+z,alpha[j])*(1.+np.power(1./(1.+zpeak[j]),alpha[j]+beta[j]))/(1.+np.power((1.+z)/(1.+zpeak[j]),alpha[j]+beta[j]))\
        for j in range(localRates.size)]))
    rates_5[i] = rates_at_z[i_5]
    rates_25[i] = rates_at_z[i_25]
    rates_50[i] = rates_at_z[i_50]
    rates_75[i] = rates_at_z[i_75]
    rates_95[i] = rates_at_z[i_95]
"""

alpha_true = 3.
beta_true = 3.
zpeak_true = 2.
localRate_true = 30.


fig,ax = plt.subplots(figsize=(5,3))
for i in range(alpha.size):   
    if localRates[i]!=0:
        ax.plot(zs,localRates[i]*np.power(1.+zs,alpha[i])*(1.+np.power(1./(1.+zpeak[i]),alpha[i]+beta[i]))/(1.+np.power((1.+zs)/(1.+zpeak[i]),alpha[i]+beta[i])),alpha=0.08,color='black',lw=0.5,zorder=0)
#ax.fill_between(zs,rates_5,rates_95,facecolor='#9ecae1',alpha=0.6)
#ax.fill_between(zs,rates_25,rates_75,facecolor='#2171b5',alpha=0.6)
#ax.plot(zs,rates_50,color='#08519c')
ax.plot(zs,localRate_true*np.power(1.+zs,alpha_true)*(1.+np.power(1./(1.+zpeak_true),alpha_true+beta_true))/(1.+np.power((1.+zs)/(1.+zpeak_true),alpha_true+beta_true)),lw=1.2,zorder=1)
ax.set_yscale('log')
ax.set_xlim([0,5])
ax.set_ylim([1e-1,1e6])
ax.xaxis.grid(True,which='major',ls=':')
ax.yaxis.grid(True,which='major',ls=':')
ax.set_ylabel(r'$\mathcal{R}(z)\,[\mathrm{Gpc}^{-3}\,\mathrm{yr}^{-1}]$',fontsize=12)
ax.set_xlabel(r'$z$',fontsize=12)
ax.tick_params(axis='both', which='major', labelsize=10, direction='in')
ax.set_axisbelow(True)
plt.minorticks_off()
plt.tight_layout()
plt.savefig('mergerRate.pdf',bbox_inches='tight')
