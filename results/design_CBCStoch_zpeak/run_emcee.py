import numpy as np
import emcee as mc
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../../code/')
from loadMockSamples import loadMockSamples
from detectionFraction import detectionFractionMaya_extended
from gwBackground import OmegaGW_massDistr
from computeTotalTime import obsTime

year = 365.25*24*3600.

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 5.
gamma_Min = -4.
gamma_Max = 12.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.

sampleDict = loadMockSamples('./../../mock_detections/mockevents_design_4096samples.h5')
nEvents = len(sampleDict)
print(nEvents)

# Compute observation time
Tobs = obsTime(nEvents,30.,1.2,45.,3.,3.,2.)
print(Tobs)

# Import redshift data (all in units of Gpc) and construct probability distribution values
ref_zs,ref_dcs,ref_dLs,ref_dVc_dz = np.loadtxt('./../../code/redshiftData.dat',usecols=(0,1,2,3),unpack=True)
low_zs = ref_zs<3.0
ref_zs = ref_zs[low_zs]
ref_dVc_dz = ref_dVc_dz[low_zs]

# Import stochastic data
freqs_stoch,orf,C_stoch,sigma2_stoch = np.loadtxt('./../../mock_detections/stochasticData_1000events_design.dat',usecols=(0,1,4,5),unpack=True)

def loglikelihood(c):

    # Read parameters
    gamma = c[0]
    mMax = c[1]
    alpha = c[2]
    beta = c[3]
    zpeak = c[4]

    if gamma<gamma_Min or gamma>gamma_Max or mMax<mMax_Min or mMax>mMax_Max or alpha<alpha_Min or alpha>alpha_Max or beta<beta_Min or beta>beta_Max or zpeak<zpeak_Min or zpeak>zpeak_Max:
        return -np.inf

    else:

        # Construct redshift probability distribution, normalize
        ref_pzs = ref_dVc_dz*np.power(1.+ref_zs,alpha-1.)/(1.+np.power((1.+ref_zs)/(1.+zpeak),alpha+beta))
        dz = ref_zs[1] - ref_zs[0]
        ref_pzs /= np.sum(ref_pzs)*dz

        logP = 0.
        for i,event in enumerate(sampleDict):

            m1_samples = sampleDict[event][0]
            z_samples = sampleDict[event][1]
            priors = sampleDict[event][2]

            # Mass 1 probability
            if gamma==1:
                p_m1 = np.power(m1_samples,-1.)/np.log(mMax/mMin)
            else:
                p_m1 = (1.-gamma)*np.power(m1_samples,-gamma)/(np.power(mMax,1.-gamma)-np.power(mMin,1.-gamma))

            # Mass 2 probability
            p_m2 = 1./(m1_samples-mMin)

            # Zero out probabilities above mMax and below mMin
            p_m1[m1_samples>mMax] = 0
            p_m1[m1_samples<mMin] = 0

            # Redshift probability
            p_z = np.interp(z_samples,ref_zs,ref_pzs)

            # Evaluate marginalized likelihood
            nSamples = m1_samples.size
            pEvidence = np.sum(p_m1*p_m2*p_z/priors)/nSamples

            # Add
            logP += np.log(pEvidence)

        # Construct CDF for total event counts
        Ntots = np.logspace(3,8,1000)
        dFraction = detectionFractionMaya_extended(mMin,mMax,gamma,alpha,beta,zpeak,nTrials=3000,znorm=True,design=True)
        #print("!!!:",dFraction,(dFraction*Ntots*np.exp(-dFraction*Ntots/nEvents))[300])
        #p_Ns = np.power(Ntots*dFraction,nEvents)*np.exp(-Ntots*dFraction)
        p_Ns_tmp = dFraction*Ntots*np.exp(-dFraction*Ntots/nEvents)
        p_Ns = np.power(p_Ns_tmp/np.max(p_Ns_tmp),nEvents)
        p_Ns /= np.sum(p_Ns)

        cdf_Ns = np.cumsum(p_Ns)

        # Get random rate and convert to local rate density
        c = np.random.random()
        totalRate = np.interp(c,cdf_Ns,Ntots)/Tobs
        pz_unnormed = ref_dVc_dz*np.power(1.+ref_zs,alpha-1)*(1.+np.power(1./(1.+zpeak),alpha+beta))/(1.+np.power((1.+ref_zs)/(1.+zpeak),alpha+beta))
        pz_integral = np.sum(pz_unnormed)*(ref_zs[1]-ref_zs[0])
        R0 = totalRate/pz_integral/(4.*np.pi)

        # Stochastic energy-density spectrum (need R0 in units of Mpc^-3 s^-1)
        OmgGW = OmegaGW_massDistr(R0/1.e9/year,alpha,beta,zpeak,gamma,mMax,freqs_stoch,freqInterp=True,mMin=5.)

        # Stochastic likelihood
        logP_stoch = np.sum(-0.5*np.power(orf*OmgGW-C_stoch,2.)/sigma2_stoch)
        logP += logP_stoch

        #print(mMax,gamma,alpha)
        print(mMax,gamma,alpha,beta,zpeak,logP,OmgGW[0],R0)
        #return logP - nEvents*np.log(detectionFractionMaya_extended(mMin,mMax,gamma,alpha,beta,zpeak,nTrials=3000,design=True))
        return logP - nEvents*np.log(dFraction)

if __name__=="__main__":

    # Initialize walkers
    nWalkers = 96
    #initial_gammas = np.random.random(nWalkers)*(gamma_Max-gamma_Min)+gamma_Min
    #initial_mMaxs = np.random.random(nWalkers)*(mMax_Max-mMax_Min)+mMax_Min
    #initial_alphas = np.random.random(nWalkers)*(alpha_Max-alpha_Min)+alpha_Min
    initial_betas = np.random.random(nWalkers)*(beta_Max-beta_Min)+beta_Min
    initial_zpeaks = np.random.random(nWalkers)*(zpeak_Max-zpeak_Min)+zpeak_Min
    initial_gammas = np.random.random(nWalkers)*(1.3-1.1)+1.1
    initial_mMaxs = np.random.random(nWalkers)*(47.-43.)+43.
    initial_alphas = np.random.random(nWalkers)*(3.2-2.8)+2.8
    #initial_betas = np.random.random(nWalkers)*(3.5-2.5)+2.5
    #initial_zpeaks = np.random.random(nWalkers)*(2.2-1.8)+1.8
    initial_walkers = np.transpose([initial_gammas,initial_mMaxs,initial_alphas,initial_betas,initial_zpeaks])
    print(initial_walkers)

    #filename = "test.h5"
    #backend = mc.backends.HDFBackend(filename)
    #backend.reset(nWalkers,5)

    # Run
    nSteps = 8000
    sampler = mc.EnsembleSampler(nWalkers,5,loglikelihood,threads=6)
    for i,result in enumerate(sampler.sample(initial_walkers,iterations=nSteps)):
        np.save('raw_samples.npy',sampler.chain)
        print("============= {0} ... {1} ============".format(i,np.mean(sampler.acceptance_fraction)))

    np.save('raw_samples.npy',sampler.chain)

    # Burn first half
    chainBurned = sampler.chain[:,int(np.floor(nSteps/2.)):,:]
    print(chainBurned)

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = 0.
    for i in range(nWalkers):
        for j in range(5):
            (tau,mean,sigma) = acor.acor(chainBurned[i,:,j])
            if tau==0 or tau!=tau:
                tau = 10
            corrTotal += tau
    meanCorLength = int(np.floor(corrTotal/(nWalkers*5)))
    print("Cl:\t",meanCorLength)

    # Down-sample by twice the mean correlation length
    chainDownsampled = chainBurned[:,::meanCorLength,:]
    print(chainDownsampled)
    print np.shape(chainDownsampled)

    # Flatten
    chainDownsampled = chainDownsampled.reshape((-1,len(chainDownsampled[0,0,:])))
    print np.shape(chainDownsampled)
    np.save('samples.npy',chainDownsampled)

    print np.mean(sampler.acceptance_fraction)


