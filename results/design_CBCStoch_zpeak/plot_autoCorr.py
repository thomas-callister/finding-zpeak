import numpy as np
import acor
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt

data = np.load('500events_raw_samples.npy','r')
goodInds = np.where(data[0,:,1]!=0)

nSteps = goodInds[0].size
nWalkers = data[:,0,0].size
nVars = data[0,0,:].size

nCutoffs = np.arange(0,nSteps+10,10)
autoCorrs = np.zeros(nCutoffs.size)

for k,nCutoff in enumerate(nCutoffs):

    # Burn first half
    chainBurned = data[:,int(np.floor(nCutoff/2.)):nCutoff,:]

    # Get mean correlation length (averaging over all variables and walkers)
    corrTotal = np.zeros(nVars)
    for i in range(nVars):
        for j in range(nWalkers):
            try:
                (tau,mean,sigma) = acor.acor(chainBurned[j,:,i])
                if tau==0 or tau!=tau:
                    tau = 10
                    #tau = np.mean(corrTotal)
            except RuntimeError:
#                print("!")
                tau = 10.
                #tau = np.mean(corrTotal)
            corrTotal[i] += 2.*tau/(nWalkers)
#            print(corrTotal,tau)
    meanCorLength = np.max(corrTotal)
    autoCorrs[k] = meanCorLength

print(np.transpose([nCutoffs,autoCorrs]))

fig,ax = plt.subplots(figsize=(12,5))
ax.plot(nCutoffs,autoCorrs)
plt.savefig('autocorrs.pdf')


