import matplotlib.style
matplotlib.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')
import matplotlib.pyplot as plt
import numpy as np

alpha = 3
beta = 3
zpeak = 2

zs = np.arange(0.,6.,0.01)
rs = np.power(1.+zs,alpha)/(1.+np.power((1.+zs)/(1.+zpeak),alpha+beta))

fig,ax = plt.subplots()
ax.plot(zs,rs)
ax.set_xlabel('Redshift $z$')
ax.set_ylabel(r'$\mathcal{R}(z)$\,[Arbitrary Units]')
ax.text(4,6.7,r'$(1+z)^{-\beta}$',fontsize=11)
ax.text(0.3,10.7,r'$(1+z)^\alpha$',fontsize=11)
ax.set_xlim(0,6)
plt.tight_layout()
plt.savefig('rateModel.pdf',bbox_inches='tight')
