import numpy as np
import matplotlib.pyplot as plt
import sys
import os
import multiprocessing as multi
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import SmoothBivariateSpline
import h5py
from mpl_toolkits.mplot3d import axes3d

# Import old pDet data
gridDataOld = h5py.File('./../../rates_pop_tutorials/redshift_fitting_maya/pdet_m1m2z.h5','r')
gridOld_ms = np.array(gridDataOld['int_ms'])
gridOld_zs = np.array(gridDataOld['int_zs'])
gridOld_pDets = np.array(gridDataOld['int_pdet_L1'])
gridOld_pDets = np.array([gridOld_pDets[i,i,:] for i in range(gridOld_ms.size)]).T
OLD_MS,OLD_ZS = np.meshgrid(gridOld_ms,gridOld_zs)

print(OLD_MS.shape,OLD_ZS.shape,gridOld_pDets.shape)

# New data
gridData = h5py.File('./../mock_detections/Pdets_O1O2.h5','r')
grid_ms = np.array(gridData['ms'])
grid_zs = np.array(gridData['zs'])
grid_pDets = np.array(gridData['pdet'])
grid_pDets = np.array([grid_pDets[i,i,:] for i in range(grid_ms.size)]).T
MS,ZS = np.meshgrid(grid_ms,grid_zs)

# New data
gridData2 = h5py.File('./../mock_detections/Pdets_design.h5','r')
grid_ms2 = np.array(gridData2['ms'])
grid_zs2 = np.array(gridData2['zs'])
grid_pDets2 = np.array(gridData2['pdet'])
grid_pDets2 = np.array([grid_pDets2[i,i,:] for i in range(grid_ms2.size)]).T
MS2,ZS2 = np.meshgrid(grid_ms2,grid_zs2)

fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
#ax.plot_wireframe(OLD_MS,OLD_ZS,np.log10(gridOld_pDets),lw=0.75)
#ax.plot_wireframe(MS,ZS,np.log10(grid_pDets),lw=0.75,color='red')
ax.plot_wireframe(OLD_MS,OLD_ZS,(gridOld_pDets),lw=0.75)
ax.plot_wireframe(MS,ZS,(grid_pDets),lw=0.75,color='red')
ax.plot_wireframe(MS2,ZS2,(grid_pDets2),lw=0.75,color='orange')
ax.view_init(elev=20., azim=50.)
plt.savefig('pdetComparison.pdf')
