import matplotlib.style
matplotlib.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import gammainc
import sys
sys.path.append('../code/')
from gwBackground import *
sys.path.append('/home/thomas.callister/Stochastic/DetectorScramble/GW-Geodesy/src/')
from makeLigoData import makeNoise
from baseline import Baseline

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('./../code/delayedRateData.npy',allow_pickle=True,encoding='latin1')[()]
formationRates = rateData['formationRates']
tdelays = rateData['tds']
zs = rateData['zs']
zformation = rateData['formationRedshifts']

"""
# Import stochastic data
freqs_O1,C_O1,sigma_O1 = np.loadtxt('../results/realSamples_CBCStoch_zpeak/Cf_O1.dat',unpack=True,skiprows=1)
freqs_O2,C_O2,sigma_O2 = np.loadtxt('../results/realSamples_CBCStoch_zpeak/Cf_O2.dat',unpack=True,skiprows=1)

# Combine
C_stoch = (C_O1/sigma_O1**2. + C_O2/sigma_O2**2.)/(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma_stoch = 1./np.sqrt(1./sigma_O1**2. + 1./sigma_O2**2.)
sigma2_stoch = np.power(sigma_stoch,2.)
freqs_stoch = freqs_O2

# Select only frequencies with data
goodInds = np.where(C_stoch==C_stoch)
freqs_stoch = freqs_stoch[goodInds]
C_stoch = C_stoch[goodInds]
sigma2_stoch = sigma2_stoch[goodInds]
"""

freqs_stoch = np.arange(10.,300.,0.25)
noise,sigma2s = makeNoise(freqs_stoch,time=3.)
ligo = Baseline.LigoBaseline()
orf = ligo.gamma_isotropic(freqs_stoch)
sigma2_stoch = sigma2s/orf**2.

testClass = OmegaGW_massDistr_timeDelay(5.,100.,zs)

lmbdas = np.linspace(-5.,3.,20)
tmaxs = np.linspace(1,13.5,20)
zthresh = 0.3

def snr(omg):
    return np.sqrt(np.sum(omg*omg/sigma2_stoch))

snrs = np.zeros((lmbdas.size,tmaxs.size))
for i,lmbda in enumerate(lmbdas):
    for j,tmax in enumerate(tmaxs):
        print(lmbda,tmax)

        fs = gammainc(0.84,(zthresh**2.)*np.power(10.,0.3*zformation))
        weightedFormationRates = formationRates*fs

        dpdt = np.power(tdelays,lmbda)
        dpdt[tdelays<0.05] = 0.
        dpdt[tdelays>tmax] = 0.
        mergerRate = weightedFormationRates.dot(dpdt)

        omg = testClass.eval(30.,mergerRate/mergerRate[0]/1.e9/year,1.2,45.,freqs_stoch)

        snrs[i,j] = snr(omg)

fig,ax = plt.subplots()

ax.xaxis.grid(True,which='major',ls=':',alpha=0.3)
ax.yaxis.grid(True,which='major',ls=':',alpha=0.3)
ax.set_xlabel(r'$\lambda$',fontsize=12)
ax.set_ylabel(r'$t_\mathrm{max}:$',fontsize=12)
ax.tick_params(labelsize=10)

#cs = ax.contour(lmbdas,tmaxs,snrs.T,levels=(0.01,0.03,0.1,0.3),colors='black')
cs = ax.contour(lmbdas,tmaxs,snrs.T,levels=(0.01,0.03,0.1,0.3,1,3,10),colors='black')
ax.clabel(cs,inline=1,fontsize=10,fmt='%.2f')

pc = ax.pcolormesh(lmbdas,tmaxs,np.log10(snrs.T),cmap='Blues')
cbar = fig.colorbar(pc,ax=ax)
cbar.set_label(label='log(SNR)',size=12)

plt.tight_layout()
plt.savefig('snrs_design_lambda_tMax.pdf',bbox_inches='tight')
plt.show()
