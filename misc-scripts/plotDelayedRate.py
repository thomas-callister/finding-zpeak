import matplotlib.style
matplotlib.style.use('/home/thomas.callister/.matplotlib/matplotlibrc')
import matplotlib.pyplot as plt
import numpy as np
from scipy.special import gammainc

# Import precomputed grid of binary formation rates as a function of merger redshift and time delay
rateData = np.load('./../code/delayedRateData.npy')[()]
formationRates = rateData['formationRates']
tdelays = rateData['tds']
zformation = rateData['zs']
print(tdelays)

# Metallicity weights
fs = gammainc(0.84,(0.1**2.)*np.power(10.,0.3*zformation))[...,None]
weightedFormationRates = formationRates*fs

lmbda = -1.
dpdt = np.power(tdelays,lmbda)
dpdt[tdelays<0.05] = 0.
dpdt[tdelays>13.5] = 0.
mergerRate = weightedFormationRates.dot(dpdt)

fig,ax = plt.subplots()
ax.plot(zformation,formationRates[:,0]/np.max(formationRates[:,0]))
ax.plot(zformation,weightedFormationRates[:,0]/np.max(weightedFormationRates[:,0]))
ax.plot(zformation,mergerRate/np.max(mergerRate))
ax.set_xlabel('Redshift $z$')
ax.set_ylabel(r'$\mathcal{R}(z)$\,[Arbitrary Units]')
#ax.text(4,6.7,r'$(1+z)^{-\beta}$',fontsize=11)
#ax.text(0.3,10.7,r'$(1+z)^\alpha$',fontsize=11)
ax.set_xlim(0,6)
plt.tight_layout()
plt.savefig('delayedRateModel.pdf',bbox_inches='tight')
