import numpy as np
import matplotlib.pyplot as plt
import sys
import h5py
sys.path.append('./../code/')
from loadSamples import loadSamples

m1_samples_150914,m2_samples_150914,z_samples_150914,priors_150914 = loadSamples("./../GWTC-1_sample_release/GW150914_GWTC-1.hdf5")

f = h5py.File('./../mock_detections/mockevents_design.h5','r')
new_zs = f['z_samples'][10,:]
weights = f['weights'][10,:]

fig,ax = plt.subplots()
ax.scatter(z_samples_150914,priors_150914,marker='x')
ax.scatter(new_zs,weights)
plt.savefig('samples.png')
