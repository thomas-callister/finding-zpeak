import numpy as np
import acor
import matplotlib.pyplot as plt
import sys
sys.path.append('./../code/')
from gwBackground import OmegaGW
sys.path.append('/home/thomas.callister/Stochastic/DetectorScramble/GW-Geodesy/src/')
from makeLigoData import makeNoise
from baseline import Baseline

# Prior bounds
mMax_Min = 30.
mMax_Max = 100.
mMin = 7.
gamma_Min = -4.
gamma_Max = 12.
alpha_Min = -25.
alpha_Max = 25.
beta_Min = 0.
beta_Max = 10.
zpeak_Min = 0.
zpeak_Max = 4.
zmax = 6.

# Get sigma2s (*not* scaled by ORF)
freqs_stoch = np.arange(10.,300.,0.25)
noise,sigma2s = makeNoise(freqs_stoch,time=3.)

# Get orf
ligo = Baseline.LigoBaseline()
orf = ligo.gamma_isotropic(freqs_stoch)

# Rescale sigma2s
sigma2_stoch = sigma2s/orf**2.


def snr(alpha,zpeak):

    # Stochastic energy-density spectrum
    beta = 3.
    OmgGW = OmegaGW(alpha,beta,zpeak,freqs_stoch)

    return np.sqrt(np.sum(np.power(OmgGW,2.)/sigma2_stoch))

if __name__=="__main__":

    alphas = np.linspace(-25,25,50)
    zpeaks = np.linspace(0.,5.,50)
    snrs = np.zeros((alphas.size,zpeaks.size))

    for i,a in enumerate(alphas):
        for j,z in enumerate(zpeaks):
            snrs[i,j] = snr(a,z)
            print(a,z,snrs[i,j])

    fig,ax = plt.subplots(figsize=(4,3))
    pc = ax.pcolormesh(alphas,zpeaks,np.log10(snrs.T),cmap='Blues',vmin=-2,vmax=3)
    cs = ax.contour(alphas,zpeaks,snrs.T,levels=(3,10),colors='black')
    ax.clabel(cs,inline=1,fontsize=10,fmt='%d',manual=[(3,1),(11,1)])
    ax.xaxis.grid(True,which='major',ls=':',alpha=0.3)
    ax.yaxis.grid(True,which='major',ls=':',alpha=0.3)
    ax.set_xlabel(r'$\alpha$',fontsize=12)
    ax.set_ylabel(r'$z_\mathrm{peak}$',fontsize=12)
    ax.tick_params(labelsize=10)

    cbar = fig.colorbar(pc,ax=ax)
    cbar.set_label(label='Stochastic SNR',size=12)
    cbar.ax.tick_params(labelsize=10)
    cbar_labels = [item.get_text() for item in cbar.ax.get_yticklabels()]
    print([int(x[1:-1]) for x in cbar_labels])
    cbar_labels = [r"$10^{{{0}}}$".format(int(x[1:-1])) for x in cbar_labels]
    cbar.ax.set_yticklabels(cbar_labels)

    plt.tight_layout()
    plt.savefig('snrs_R50_design_3years.pdf',bbox_inches='tight')
