import numpy as np
from scipy.interpolate import LinearNDInterpolator

# Constants
Mpc = 3.09e16*1e6
km = 1.e3
H0 = 67.7*km/Mpc # Units of 1/s
Gyr = 1.e9*365.*24*3600
yr = 365.*24*3600
OmgM = 0.3
OmgL = 0.7

def timeDelay(zMerge,zForm):

    if zMerge>zForm:
        return 0
    else:
        zs = np.linspace(zMerge,zForm,200)
        timeDelayIntegrands = (1./Gyr)*np.power((1.+zs)*H0*np.sqrt(OmgM*np.power(1.+zs,3.)+OmgL),-1.)
        return np.sum(timeDelayIntegrands)*(zs[1]-zs[0])

"""
zData = []
tData = []
for zm in np.arange(0.,10.,0.01):
    for zf in np.arange(0.,10.,0.01):

        zData.append((zm,timeDelay(zm,zf)))
        tData.append(zf)

dataDict = {}
dataDict['zData'] = zData
dataDict['tData'] = tData
np.save("tdelayData.npy",dataDict)
"""

