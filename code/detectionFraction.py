import numpy as np
import sys
import os
import multiprocessing as multi
from scipy.interpolate import LinearNDInterpolator
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import SmoothBivariateSpline
#import lal
#import lalsimulation as ls
import h5py

codeDir = os.path.dirname(os.path.realpath(__file__))

Mpc = 3.09e16*1e6
Gpc = Mpc*1e3
Msun = 1.99e30
c = 2.998e8
G = 6.67e-11
MsunToSec = Msun*G/np.power(c,3.)

def detectionFractionMaya(mMin,mMax,a,lmbda,nTrials=16384,znorm=False):

    # Import redshift data (all in units of Gpc)
    ref_zs,dcs,dLs,ref_dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)

    # Import Pdet grid
    #gridData = h5py.File('{0}/../../rates_pop_tutorials/redshift_fitting_maya/pdet_m1m2z.h5'.format(codeDir),'r')
    #grid_ms = np.array(gridData['int_ms'])
    #grid_zs = np.array(gridData['int_zs'])
    #grid_pDets = np.array(gridData['int_pdet_L1'])
    gridData = h5py.File('{0}/../mock_detections/Pdets_design.h5'.format(codeDir),'r')
    grid_ms = np.array(gridData['ms'])
    grid_zs = np.array(gridData['zs'])
    grid_pDets = np.array(gridData['pdet'])
        
    # Construct redshift probability distribution
    dz = grid_zs[1]-grid_zs[0]
    dVc_dz = np.interp(grid_zs,ref_zs,ref_dVc_dz)
    pzs = dVc_dz*np.power(1.+grid_zs,lmbda-1.)
    if znorm:
        pzs /= np.trapz(pzs,grid_zs)

    # Loop across redshifts
    fracs = np.zeros(grid_zs.size)
    for i,z in enumerate(grid_zs):

        # Draw random m1's
        cdfs = np.random.random(nTrials)
        if a==1:
            m1s = mMin*np.power(mMax/mMin,cdfs)
        else:
            m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-a)),1./(1.-a))

        # Draw random m2's, uniform between mMin and m1
        cdfs = np.random.random(nTrials)
        m2s = mMin + cdfs*(m1s-mMin)

        # Fill in lower half of triangle
        grid_pDets[:,:,i] = np.tril(grid_pDets[:,:,i].T,-1)+grid_pDets[:,:,i]

        # Interpolate
        pDet_spline = RectBivariateSpline(grid_ms,grid_ms,grid_pDets[:,:,i],s=0)
        fracs[i] = np.mean(pDet_spline(m1s,m2s,grid=False))

    # Redshift probability weighting
    fractionDetected = np.sum(fracs*pzs)*dz
    return fractionDetected

def detectionFractionMaya_extended(mMin,mMax,gamma,alpha,beta,zpeak,nTrials=16384,znorm=False,design=False):

    # Import Pdet grid
    #gridData = h5py.File('{0}/../../rates_pop_tutorials/redshift_fitting_maya/pdet_m1m2z.h5'.format(codeDir),'r')
    #grid_ms = np.array(gridData['int_ms'])
    #grid_zs = np.array(gridData['int_zs'])
    #grid_pDets = np.array(gridData['int_pdet_L1'])
    if design:
        gridData = h5py.File('{0}/../mock_detections/Pdets_design.h5'.format(codeDir),'r')
    else:
        gridData = h5py.File('{0}/../mock_detections/Pdets_O1O2.h5'.format(codeDir),'r')
    grid_ms = np.array(gridData['ms'])
    grid_zs = np.array(gridData['zs'])
    grid_pDets = np.array(gridData['pdet'])

    # Import redshift data (all in units of Gpc)
    ref_zs,dcs,dLs,ref_dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)
    dVc_dz = np.interp(grid_zs,ref_zs,ref_dVc_dz)

    # Construct redshift probability distribution
    dz = grid_zs[1]-grid_zs[0]
    pzs = dVc_dz*np.power(1.+grid_zs,alpha-1.)/(1.+np.power((1.+grid_zs)/(1.+zpeak),alpha+beta))
    if znorm:
        pzs /= np.trapz(pzs,grid_zs)
    #pzs = dVc_dz*np.power(1.+grid_zs,alpha-1.)

    # Loop across redshifts
    fracs = np.zeros(grid_zs.size)
    for i,z in enumerate(grid_zs):

        # Draw random m1's
        cdfs = np.random.random(nTrials)
        if gamma==1:
            m1s = mMin*np.power(mMax/mMin,cdfs)
        else:
            m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-gamma)),1./(1.-gamma))

        # Draw random m2's, uniform between mMin and m1
        cdfs = np.random.random(nTrials)
        m2s = mMin + cdfs*(m1s-mMin)

        # Fill in lower half of triangle
        grid_pDets[:,:,i] = np.tril(grid_pDets[:,:,i].T,-1)+grid_pDets[:,:,i]

        # Interpolate
        pDet_spline = RectBivariateSpline(grid_ms,grid_ms,grid_pDets[:,:,i],s=0)
        fracs[i] = max(0.,np.mean(pDet_spline(m1s,m2s,grid=False)))
        #if fracs[i]<0.:
        #    print("!!!!",i,fracs[i],np.max(grid_ms),np.max(m1s))

    # Redshift probability weighting
    fractionDetected = np.sum(fracs*pzs)*dz
    return fractionDetected

def detectionFractionMaya_timeDelay(mMin,mMax,gamma,ref_zs,ref_pzs,nTrials=16384,znorm=False,design=False):

    # Import Pdet grid
    if design:
        gridData = h5py.File('{0}/../mock_detections/Pdets_design.h5'.format(codeDir),'r')
    else:
        gridData = h5py.File('{0}/../mock_detections/Pdets_O1O2.h5'.format(codeDir),'r')
    grid_ms = np.array(gridData['ms'])
    grid_zs = np.array(gridData['zs'])
    grid_pDets = np.array(gridData['pdet'])
    dz = grid_zs[1]-grid_zs[0]

    # Import redshift data (all in units of Gpc)
    #ref_zs,dcs,dLs,ref_dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)
    #dVc_dz = np.interp(grid_zs,ref_zs,ref_dVc_dz)

    # Construct redshift probability distribution
    pzs = np.interp(grid_zs,ref_zs,ref_pzs)
    if znorm:
        pzs /= np.trapz(pzs,grid_zs)

    # Loop across redshifts
    fracs = np.zeros(grid_zs.size)
    for i,z in enumerate(grid_zs):

        # Draw random m1's
        cdfs = np.random.random(nTrials)
        if gamma==1:
            m1s = mMin*np.power(mMax/mMin,cdfs)
        else:
            m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-gamma)),1./(1.-gamma))

        # Draw random m2's, uniform between mMin and m1
        cdfs = np.random.random(nTrials)
        m2s = mMin + cdfs*(m1s-mMin)

        # Fill in lower half of triangle
        grid_pDets[:,:,i] = np.tril(grid_pDets[:,:,i].T,-1)+grid_pDets[:,:,i]

        # Interpolate
        pDet_spline = RectBivariateSpline(grid_ms,grid_ms,grid_pDets[:,:,i],s=0)
        fracs[i] = np.mean(pDet_spline(m1s,m2s,grid=False))

    # Redshift probability weighting
    fractionDetected = np.sum(fracs*pzs)*dz
    return fractionDetected

def detectionFractionMaya_massEvolution(mMin,mMax0,gamma1,gamma2,alpha,nTrials=16384,znorm=False):

    # Import redshift data (all in units of Gpc)
    ref_zs,dcs,dLs,ref_dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)

    # Import Pdet grid
    gridData = h5py.File('{0}/../../rates_pop_tutorials/redshift_fitting_maya/pdet_m1m2z.h5'.format(codeDir),'r')
    grid_ms = np.array(gridData['int_ms'])
    grid_zs = np.array(gridData['int_zs'])
    grid_pDets = np.array(gridData['int_pdet_L1'])
        
    # Construct redshift probability distribution
    dz = grid_zs[1]-grid_zs[0]
    dVc_dz = np.interp(grid_zs,ref_zs,ref_dVc_dz)
    pzs = dVc_dz*np.power(1.+grid_zs,alpha-1.)
    if znorm:
        print("normalizing...")
        pzs /= np.trapz(pzs,grid_zs)

    # Loop across redshifts
    fracs = np.zeros(grid_zs.size)
    for i,z in enumerate(grid_zs):

        # Maximum mass at redshift z
        mMax = mMax0*np.power(1.+z,gamma2)

        # Draw random m1's
        cdfs = np.random.random(nTrials)
        if gamma1==1:
            m1s = mMin*np.power(mMax/mMin,cdfs)
        else:
            m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-gamma1)),1./(1.-gamma1))

        # Draw random m2's, uniform between mMin and m1
        cdfs = np.random.random(nTrials)
        m2s = mMin + cdfs*(m1s-mMin)

        # Fill in lower half of triangle
        grid_pDets[:,:,i] = np.tril(grid_pDets[:,:,i].T,-1)+grid_pDets[:,:,i]

        # Interpolate
        pDet_spline = RectBivariateSpline(grid_ms,grid_ms,grid_pDets[:,:,i],s=0)
        fracs[i] = np.mean(pDet_spline(m1s,m2s,grid=False))

    # Redshift probability weighting
    fractionDetected = np.sum(fracs*pzs)*dz
    return fractionDetected

def detectionFractionMaya_massEvolution_extended(mMin,mMax0,gamma1,gamma2,alpha,beta,zpeak,nTrials=16384,znorm=False):

    # Import redshift data (all in units of Gpc)
    ref_zs,dcs,dLs,ref_dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)

    # Import Pdet grid
    gridData = h5py.File('{0}/../../rates_pop_tutorials/redshift_fitting_maya/pdet_m1m2z.h5'.format(codeDir),'r')
    grid_ms = np.array(gridData['int_ms'])
    grid_zs = np.array(gridData['int_zs'])
    grid_pDets = np.array(gridData['int_pdet_L1'])
        
    # Construct redshift probability distribution
    dz = grid_zs[1]-grid_zs[0]
    dVc_dz = np.interp(grid_zs,ref_zs,ref_dVc_dz)
    pzs = dVc_dz*np.power(1.+grid_zs,alpha-1.)/(1.+np.power((1.+grid_zs)/(1.+zpeak),alpha+beta))
    if znorm:
        print("normalizing...")
        pzs /= np.trapz(pzs,grid_zs)

    # Loop across redshifts
    fracs = np.zeros(grid_zs.size)
    for i,z in enumerate(grid_zs):

        # Maximum mass at redshift z
        mMax = mMax0*np.power(1.+z,gamma2)

        # Draw random m1's
        cdfs = np.random.random(nTrials)
        if gamma1==1:
            m1s = mMin*np.power(mMax/mMin,cdfs)
        else:
            m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-gamma1)),1./(1.-gamma1))

        # Draw random m2's, uniform between mMin and m1
        cdfs = np.random.random(nTrials)
        m2s = mMin + cdfs*(m1s-mMin)

        # Fill in lower half of triangle
        grid_pDets[:,:,i] = np.tril(grid_pDets[:,:,i].T,-1)+grid_pDets[:,:,i]

        # Interpolate
        pDet_spline = RectBivariateSpline(grid_ms,grid_ms,grid_pDets[:,:,i],s=0)
        fracs[i] = np.mean(pDet_spline(m1s,m2s,grid=False))

    # Redshift probability weighting
    fractionDetected = np.sum(fracs*pzs)*dz
    return fractionDetected

if __name__=="__main__":

    #f = detectionFractionMaya(6.,100.,2.,0.2)
    #print(f)

    """

    alphas = np.linspace(-4.,12.,20)
    lambdas = np.linspace(-25.,25.,20)
    mMaxs = np.linspace(30.,100.,20)
    coords = []
    logfracs = []
    for a in alphas:
        for l in lambdas:
            for mMax in mMaxs:
                print(a,l,mMax)
                coords.append([a,l,mMax])
                logfracs.append(np.log(detectionFractionMaya(7.,mMax,a,l)))

    resultsDict = {'coords':coords,'logfracs':logfracs} 
    np.save('detectionFractionData.npy',resultsDict)

    resultsDict = np.load('detectionFractionData.npy')[()]
    coords = resultsDict['coords']
    logfracs = resultsDict['logfracs']

    test = LinearNDInterpolator(coords,logfracs,rescale=True)
    print(test([0.,10.,40.]))
    print(np.log(detectionFractionMaya(7.,40.,0.,10.)))

    """ 

    estimates = np.zeros(20)
    for i in range(estimates.size):
        estimate = np.log(detectionFractionMaya_extended(5.,45.,1.2,3.,3.,3.0,nTrials=3000,design=True))
        print(estimate)
        estimates[i] = estimate
    print(np.mean(estimates),np.std(estimates))
