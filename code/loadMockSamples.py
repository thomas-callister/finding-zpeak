import h5py
import numpy as np
import os
from scipy.stats import gaussian_kde

codeDir = os.path.dirname(os.path.realpath(__file__))

c = 3e8
Mpc = 3.09e16*1e6
km = 1.e3
H0 = 67.7*km*1.e3 # Units of m/s/Gpc
OmgM = 0.31
OmgL = 0.69

def genSamples(m1_true,m1_samples,z_true,z_samples):

    # Generate KDE
    kde = gaussian_kde([m1_samples,z_samples],bw_method=m1_samples.size**(-2./6.))

    # Draw new random samples until we get one smaller than the true mass
    m1_mean = np.mean(m1_samples)
    m1_std = np.std(m1_samples)
    z_mean = np.mean(z_samples)
    z_std = np.std(z_samples)
    m1_sample_min = np.min(m1_samples)
    z_sample_min = np.min(z_samples)
    z_sample_max = np.max(z_samples)
    success=True
    #while m1_sample_min>=m1_true or m1_samples.size<1024:
    #while m1_sample_min>=m1_true or z_sample_min>=z_true or z_sample_max<=z_true:
    #while np.min(m1_samples)>=45.:

    inds = np.where(m1_samples<=45.)[0]
    while inds.size<=10:

        # Random mass and redshift
        new_m1 = (8.*m1_std)*np.random.random()+max(m1_mean-4.*m1_std,5.)
        new_z = (8.*z_std)*np.random.random()+(z_mean-4.*z_std)

        # Choose to store (or not) using rejection sampling
        if np.random.random()<=kde([new_m1,new_z])/(10./(8.*m1_std*8.*z_std)):

            m1_samples = np.append(m1_samples,new_m1)
            z_samples = np.append(z_samples,new_z)
            m1_sample_min = min(new_m1,m1_sample_min)
            z_sample_min = min(new_z,z_sample_min)
            z_sample_max = max(new_z,z_sample_max)

        if m1_samples.size>=30000:
            success=False
            break
    
        inds = np.where(m1_samples<=45.)[0]

    return m1_samples,z_samples,success 

def loadMockSamples(sampleFile,nEvents=500,saveDict=None,zMax=None):

    # Load mock events, get total number
    f = h5py.File(sampleFile,'r')
    totEvents = f['m1_detected'].size
    print(totEvents)

    print(np.array(f['m2_detected']))

    # Import redshift data, used to map between redshifts, comoving distances, and luminosity distances
    reference_zs,reference_dLs = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,2),unpack=True)

    # Loop across events
    #sampleDict = {'N':nEvents}
    #print(sampleDict['N'])
    sampleDict = {}
    for i in range(nEvents):

        # Load mass samples, check that there's support below 45 Msun
        #if np.min(f['z_samples'][i,:])<0.:
        #    print("!!!!",i,np.max(f['z_samples'][i,:]))
        #    continue

        m1_samples = f['m1_samples'][i,:]
        z_samples = f['z_samples'][i,:]
        m1_true = f['m1_detected'][i]
        z_true = f['z_detected'][i]

        success=True
        #if m1_true<=np.min(m1_samples):
        #    print("!!!!!!!",i,m1_true,np.min(m1_samples))
        #if m1_true>=np.max(m1_samples):
        #    print("!!!!!!!",i,m1_true,np.max(m1_samples))
        inds = np.where(m1_samples<=45.)[0]
        if inds.size<=5:
            print("!!!!!!!!",m1_true,np.min(m1_samples),inds.size)

        #if z_true>1.5:
        #    print(i,z_true)
        #    continue
            #m1_samples,z_sampls,success = genSamples(m1_true,m1_samples,z_true,z_samples)
        #    #print(m1_samples.size,success)
        #    continue
        #if np.min(m1_samples)>45:
        #    print("!!!!!!",m1_true,np.min(m1_samples))
        #    continue
        #if z_true<=np.min(z_samples):
        #    print("!!!!!!1",i,z_true,np.min(z_samples))
        #if z_true>=np.max(z_samples):
        #    print("!!!!!!1",i,z_true,np.max(z_samples))
        #print("Old: ",i,m1_true,np.min(m1_samples),z_true,np.max(z_samples),m1_samples.size)
        #m1_samples,z_samples,success = genSamples(m1_true,m1_samples,z_true,z_samples)
        #print("New: ",i,m1_true,np.min(m1_samples),z_true,np.max(z_samples),m1_samples.size)

        if success==False:
            print("Tossing event............")
            continue

        #if np.min(m1_samples)>45.:
        #    print(i,np.min(m1_samples))
        #    continue

        if zMax:
            if z_true>zMax:
                print("Too far!!",z_true)
                continue

        # Compute comoving and luminosity distances corresponding to redshift samples
        dLs = np.interp(z_samples,reference_zs,reference_dLs)
        dcs = dLs/(1.+z_samples)

        # Compute lalinference priors and reweight by Maya's weights
        z_priors = np.power(dLs,2.)*np.power(1.+z_samples,2.)*(dcs+(1.+z_samples)*(c/H0)/np.sqrt(OmgM*np.power(1.+z_samples,3.)+OmgL))
        z_priors /= f['weights'][i,:]
        #z_priors = np.ones(z_samples.size)

        # Store
        sampleDict[i] = np.array([m1_samples,z_samples,z_priors])

    print(len(sampleDict))

    if saveDict:
        np.save(saveDict,sampleDict)

    return sampleDict

if __name__=="__main__":
    #events = loadMockSamples('./../mock_detections/design_alpha3.0_beta3.0_zpeak1.0/mockevents_design.h5.run3',1000)
    events = loadMockSamples('./../mock_detections/mockevents_design.h5',500)
    print(len(events))
