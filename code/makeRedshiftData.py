import numpy as np

# Constants
c = 3e8
Mpc = 3.09e16*1e6
km = 1.e3
H0 = 67.7*km # Units of m/s/Mpc
OmgM = 0.31
OmgL = 0.69

# Define array of redshifts
dz = 0.01
zs = np.arange(0.,10.0,dz)
#zs = np.append(np.arange(0.,0.2,.001),10.**np.arange(np.log10(0.2),1,0.005))
#dz = np.diff(zs)
#zs = zs[:-1]
#dlogz = 0.001
#zs = 10.**(np.arange(-4.,1.,dlogz))
#print(zs)

# Integrand of comoving distance integral
#dc_integrands = (c/H0)*(zs*np.log(10)*dlogz)/np.sqrt(OmgM*np.power(1.+zs,3.)+OmgL)
dc_integrands = (c/H0)*dz/np.sqrt(OmgM*np.power(1.+zs,3.)+OmgL)
dcs = np.insert(np.cumsum(dc_integrands),0,0)[:-1]
#dcs = np.cumsum(dc_integrands)

# Luminosity distances
dLs = dcs*(1.+zs)

# Convert to Gpc
dcs = dcs/1000.
dLs = dLs/1000.

# d(Vc)/dz
dVc_dz = np.power(dcs,2.)*(c/H0/1000)*(1./np.sqrt(OmgM*np.power(1.+zs,3.)+OmgL))

# Save
np.savetxt("redshiftData.dat",np.transpose([zs,dcs,dLs,dVc_dz]),delimiter='\t',header='z\tdc\tdL\tdVc_dz')
