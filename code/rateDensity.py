import numpy as np
import os
from detectionFraction import detectionFractionMaya
from scipy.interpolate import LinearNDInterpolator
import matplotlib.pyplot as plt

Nobs = 10.
Tobs = 169.7/365.25
mMin = 7.

def rateDensity(sampleFile):

    # Load posterior samples on shape parameters
    data = np.load(sampleFile)
    alpha = data[:,0]
    lmbda = data[:,1]
    mMax = data[:,2]
    print(mMax.size)

    # Load redshift data (all in units of Gpc)
    codeDir = os.path.dirname(os.path.realpath(__file__))
    zs,dcs,dLs,dVc_dz = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,1,2,3),unpack=True)

    # Get detection fractions
    dfracDict = np.load('{0}/detectionFractionData.npy'.format(codeDir))[()]
    coords = dfracDict['coords']
    logfracs = dfracDict['logfracs']
    logdfrac = LinearNDInterpolator(coords,logfracs,rescale=True)
    detFractions = np.array([np.exp(logdfrac(alpha[i],lmbda[i],mMax[i])) for i in range(alpha.size)])

    # For every sample, compute probability distribution on Ntotal
    Ntotal = np.logspace(0.5,10.,100)
    dlogN = np.log(Ntotal[1])-np.log(Ntotal[0])
    p_Ntotal = np.zeros((alpha.size,Ntotal))
    for i in range(alpha.size):

        # Compute conditional probabilities on Ntotals, assuming the given sample's shape parameters
        p_Ntotal = np.power(Ntotal*detFractions[i],Nobs)*np.exp(-Ntotal*detFractions[i])
        p_Ntotal /= np.sum(p_Ntotal[i,:]/Ntotal)*dlogN

        # Compute local merger rate (note that dVc/dz's in p(z) and dN/dVdt cancel)
        R0s = Ntotal*


    # Compute probability of total number of events Ntotal
    Ntotal = np.logspace(0.5,10.,10)
    p_Ntotal = np.zeros(Ntotal.size)
    for i,n in enumerate(Ntotal):
        #p_Ntotal[i] = np.sum((1./n)*np.power(n*detFractions,Nobs)*np.exp(-n*detFractions))
        p_Ntotal[i] = np.sum(np.power(n*detFractions,Nobs)*np.exp(-n*detFractions))

    iMax = np.argmax(p_Ntotal) 
    Nmax = Ntotal[iMax]

    fig,ax = plt.subplots(figsize=(4,3))
    R0zs = np.zeros(alpha.size)
    for i in range(0,alpha.size):

        # dp/dz
        pzs = dVc_dz*np.power(1.+zs,lmbda[i]-1.)
        pzs /= np.trapz(pzs,zs)

        Rzs = Nmax*pzs*(1./dVc_dz)*(1.+zs)/Tobs
        R0zs[i] = Rzs[1]

        ax.plot(zs,Nmax*pzs*(1./dVc_dz)*(1.+zs)/Tobs,color='grey',alpha=0.1)
        ax.set_yscale('log')

    plt.savefig('rateDensity.pdf')

    print(np.median(R0zs))



if __name__=="__main__":

    rateDensity('./../realSamples/samples.npy')
