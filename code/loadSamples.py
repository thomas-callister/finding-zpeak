import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import gaussian_kde
import os

codeDir = os.path.dirname(os.path.realpath(__file__))

c = 3e8
Mpc = 3.09e16*1e6
km = 1.e3
H0 = 67.7*km*1.e3 # Units of m/s/Gpc
OmgM = 0.31
OmgL = 0.69

def loadSamples(h5file):

    # Load data
    h5 = h5py.File(h5file,'r')

    # Detector frame masses and luminosity distance (convert to Gpc)
    m1s_detector = h5['IMRPhenomPv2_posterior']['m1_detector_frame_Msun']
    m2s_detector = h5['IMRPhenomPv2_posterior']['m2_detector_frame_Msun']
    dLs = h5['IMRPhenomPv2_posterior']['luminosity_distance_Mpc']/1.e3

    # Import redshift data, compute redshift and comoving distances
    reference_zs,reference_dLs = np.loadtxt('{0}/redshiftData.dat'.format(codeDir),usecols=(0,2),unpack=True)
    zs = np.interp(dLs,reference_dLs,reference_zs)
    dcs = dLs/(1.+zs)

    # Convert to source frame masses
    m1s_source = m1s_detector/(1.+zs)
    m2s_source = m2s_detector/(1.+zs)

    print(h5file)
    print(np.median(m1s_source),np.median(m2s_source),np.median(zs))

    # Priors on distance
    #z_priors = np.power(dLs,2.)
    z_priors = np.power(dLs,2.)*np.power(1.+zs,2.)*(dcs+(1.+zs)*(c/H0)/np.sqrt(OmgM*np.power(1.+zs,3.)+OmgL))

    return m1s_source,m2s_source,zs,z_priors

if __name__=="__main__":

    loadSamples('../realSamples/GWTC-1_sample_release/GW150914_GWTC-1.hdf5')
