import numpy as np
import time
import sys
import os
import matplotlib.pyplot as plt

codeDir = os.path.dirname(os.path.realpath(__file__))

year = 365.25*24*3600.
Mpc = 3.09e16*1e6
Gpc = Mpc*1e3
Msun = 1.99e30
c = 2.998e8
G = 6.67e-11
MsunToSec = Msun*G/np.power(c,3.)
OmgM = 0.31
OmgL = 0.69
H0 = 67.7*(10.**3.)/Mpc # SI units
R0_example = 50./year/np.power(1.e3,3) # 1/Gpc^3/s converted to 1/Mpc^3/s
rhoC = 3.*np.power(H0*c,2.)/(8.*np.pi*G)*np.power(Mpc,3.) # Converted to J/Mpc^3

# Load PSD data
psdFreqs,psdVals = np.loadtxt('{0}/aligo_O3low.txt'.format(codeDir),unpack=True)

def dEdf(Mtot,freqs,eta=0.25):

    # Define IMR parameters
    fMerge = (0.29740*eta**2. + 0.044810*eta + 0.095560)/(np.pi*Mtot*MsunToSec)
    fRing = (0.59411*eta**2. + 0.089794*eta + 0.19111)/(np.pi*Mtot*MsunToSec)
    fCut = (0.84845*eta**2. + 0.12828*eta + 0.27299)/(np.pi*Mtot*MsunToSec)
    sigma = (0.50801*eta**2. + 0.077515*eta + 0.022369)/(np.pi*Mtot*MsunToSec)

    # Initialize energy density, identify piecewise components
    dEdf_spectrum = np.zeros(freqs.shape)
    inspiral = freqs<fMerge
    merger = (freqs>=fMerge)*(freqs<fRing)
    ringdown = (freqs>=fRing)*(freqs<fCut)

    # Fill
    dEdf_spectrum[inspiral] = np.power(freqs[inspiral],-1./3.)
    dEdf_spectrum[merger] = np.power(freqs[merger],2./3.)/fMerge
    dEdf_spectrum[ringdown] = np.square(freqs[ringdown]/(1.+np.square((freqs[ringdown]-fRing)/(sigma/2.))))/(fMerge*fRing**(4./3.))

    # Normalization
    Mc = np.power(eta,3./5.)*Mtot*Msun
    amp = np.power(G*np.pi,2./3.)*np.power(Mc,5./3.)/3.

    return amp*dEdf_spectrum

def dPdz_monotonic(zs,alpha):
    pzs = np.power(1.+zs,alpha)
    pzs /= pzs[0]
    return pzs

def dPdz(zs,alpha,beta,zp):
    pzs = np.power(1.+zs,alpha)/(1.+np.power((1.+zs)/(1.+zp),alpha+beta))
    pzs /= pzs[0]
    return pzs

def OmegaGW(R0,alpha,beta,zp,freqs):

    zs = np.arange(0.,10.,0.05)
    dz = zs[1]-zs[0]

    # Evaluate energy density of singe object
    redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])
    dedf = dEdf(30.,redshiftedFreqs).T

    # Redshift integrand
    R_invE = R0*dPdz(zs,alpha,beta,zp)/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz
    return Omg_spectrum

def OmegaGW_massDistr(R0,alpha,beta,zp,gamma,mMax,targetFreqs,freqInterp=False,mMin=7.):

    if freqInterp:
        freqs = np.linspace(10.,1000.,200)
    else:
        freqs = targetFreqs

    # Set up mass grid
    Mtots = np.linspace(2.*mMin,2.*mMax,20)
    qs = np.linspace(mMin/mMax,1,22)
    etas = qs/(1.+qs)/(1.+qs)

    zs = np.arange(0.,10.,0.05)
    dz = zs[1]-zs[0]
    redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])

    # Compute energy spectra
    energySpectra = np.array([[dEdf(M,redshiftedFreqs,eta=eta) for M in Mtots] for eta in etas])

    # Grid
    Mtots_2d,qs_2d = np.meshgrid(Mtots,qs)
    m1s_2d = Mtots_2d/(1.+qs_2d)
    m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

    # Compute probabilities, cut on masses
    probs = 1./(1.+qs_2d)*np.power(m1s_2d,-gamma)
    probs[m1s_2d>mMax] = 0
    probs[m1s_2d<mMin] = 0
    probs[m2s_2d>mMax] = 0
    probs[m2s_2d<mMin] = 0
    probs /= np.sum(probs)
    
    # Compute weighted average of energy-density spectrum
    dedf = np.tensordot(probs,energySpectra,axes=2).T

    # Redshift integrand
    R_invE = R0*dPdz(zs,alpha,beta,zp)/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz

    if freqInterp:
        final_Omg_spectrum = np.interp(targetFreqs,freqs,Omg_spectrum,left=0.,right=0.)
    else:
        final_Omg_spectrum = Omg_spectrum

    return final_Omg_spectrum

def OmegaGW_monotonic(R0,alpha,gamma,mMax,targetFreqs,freqInterp=False):

    if freqInterp:
        freqs = np.linspace(1.,500.,100)
    else:
        freqs = targetFreqs

    mMin = 7.

    # Set up mass grid
    Mtots = np.linspace(2.*mMin,2.*mMax,30)
    qs = np.linspace(mMin/mMax,1,30)
    etas = qs/np.power(1.+qs,2.)

    zs = np.arange(0.,10.,0.05)
    dz = zs[1]-zs[0]
    redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])

    # Compute energy spectra
    energySpectra = np.array([[dEdf(M,redshiftedFreqs,eta=eta) for M in Mtots] for eta in etas])

    # Grid
    Mtots_2d,qs_2d = np.meshgrid(Mtots,qs)
    m1s_2d = Mtots_2d/(1.+qs_2d)
    m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

    # Compute probabilities, cut on masses
    probs = 1./(1.+qs_2d)*np.power(m1s_2d,-gamma)\
                *np.heaviside(mMax-m1s_2d,0.5)\
                *np.heaviside(mMax-m2s_2d,0.5)\
                *np.heaviside(m1s_2d-mMin,0.5)\
                *np.heaviside(m2s_2d-mMin,0.5)
    probs /= np.sum(probs)
    
    # Compute weighted average of energy-density spectrum
    dedf = np.tensordot(probs,energySpectra,axes=2).T

    # Redshift integrand
    R_invE = R0*dPdz_monotonic(zs,alpha)/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz

    if freqInterp:
        final_Omg_spectrum = np.interp(targetFreqs,freqs,Omg_spectrum,left=0.,right=0.)
    else:
        final_Omg_spectrum = Omg_spectrum

    return final_Omg_spectrum

class OmegaGW_massDistr_timeDelay():

    def __init__(self,ref_mMin,ref_mMax,ref_zs):

        self.ref_zs = ref_zs

        # Initialize grid of masses to save
        self.ref_mMin = ref_mMin
        self.ref_mMax = ref_mMax
        self.ref_Mtots = np.linspace(2.*self.ref_mMin,2.*self.ref_mMax,40)
        self.ref_qs = np.linspace(0.05,1,42)

        # Grid
        self.Mtots_2d,self.qs_2d = np.meshgrid(self.ref_Mtots,self.ref_qs)
        self.m1s_2d = self.Mtots_2d/(1.+self.qs_2d)
        self.m2s_2d = self.qs_2d*self.Mtots_2d/(1.+self.qs_2d)

        self.ref_etas = self.ref_qs/(1.+self.ref_qs)/(1.+self.ref_qs)
        self.ref_freqs = np.linspace(10.,500.,200)
        self.ref_redshiftedFreqs = np.array([self.ref_freqs*(1.+z) for z in self.ref_zs])
        self.ref_energySpectra = np.array([[dEdf(M,self.ref_redshiftedFreqs,eta=eta) for M in self.ref_Mtots] for eta in self.ref_etas])

    def eval(self,R0,dRdV_unnorm,gamma,mMax,targetFreqs):

        dz = self.ref_zs[1]-self.ref_zs[0]

        # Compute probabilities, cut on masses
        probs = 1./(1.+self.qs_2d)*np.power(self.m1s_2d,-gamma)
        probs[self.m1s_2d>mMax] = 0
        probs[self.m1s_2d<self.ref_mMin] = 0
        probs[self.m2s_2d>mMax] = 0
        probs[self.m2s_2d<self.ref_mMin] = 0
        probs /= np.sum(probs)
    
        # Compute weighted average of energy-density spectrum
        dedf = np.tensordot(probs,self.ref_energySpectra,axes=2).T

        # Redshift integrand
        R_invE = R0*dRdV_unnorm/np.sqrt(OmgM*(1.+self.ref_zs)**3.+OmgL)/(1.+self.ref_zs)

        # Convert to energy density
        Omg_spectrum = (self.ref_freqs/rhoC/H0)*dedf.dot(R_invE)*dz
        final_Omg_spectrum = np.interp(targetFreqs,self.ref_freqs,Omg_spectrum,left=0.,right=0.)

        return final_Omg_spectrum

def OmegaGW_massDistr_timeDelay_direct(R0,dRdV_unnorm,zs,gamma,mMax,targetFreqs,freqInterp=False):

    dz = zs[1]-zs[0]

    if freqInterp:

        # Grid
        Mtots_2d,qs_2d = np.meshgrid(ref_Mtots,ref_qs)
        m1s_2d = Mtots_2d/(1.+qs_2d)
        m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

        mMin = ref_mMin
        energySpectra = ref_energySpectra
        freqs = ref_freqs

    else:
        freqs = targetFreqs

        mMin = 5.

        # Set up mass grid
        Mtots = np.linspace(2.*mMin,2.*mMax,20)
        qs = np.linspace(mMin/mMax,1,22)
        etas = qs/(1.+qs)/(1.+qs)

        redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])

        # Compute energy spectra
        energySpectra = np.array([[dEdf(M,redshiftedFreqs,eta=eta) for M in Mtots] for eta in etas])

        # Grid
        Mtots_2d,qs_2d = np.meshgrid(Mtots,qs)
        m1s_2d = Mtots_2d/(1.+qs_2d)
        m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

    # Compute probabilities, cut on masses
    probs = 1./(1.+qs_2d)*np.power(m1s_2d,-gamma)
    probs[m1s_2d>mMax] = 0
    probs[m1s_2d<mMin] = 0
    probs[m2s_2d>mMax] = 0
    probs[m2s_2d<mMin] = 0
    probs /= np.sum(probs)
    
    # Compute weighted average of energy-density spectrum
    dedf = np.tensordot(probs,energySpectra,axes=2).T

    # Redshift integrand
    R_invE = R0*dRdV_unnorm/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz

    if freqInterp:
        final_Omg_spectrum = np.interp(targetFreqs,freqs,Omg_spectrum,left=0.,right=0.)
    else:
        final_Omg_spectrum = Omg_spectrum

    return final_Omg_spectrum

def OmegaGW_massEvolution(R0,alpha,gamma1,gamma2,mMax,targetFreqs,freqInterp=False,mMax_cutoff=300.):

    if freqInterp:
        freqs = np.linspace(10.,500.,100)
    else:
        freqs = targetFreqs

    # Build 2D array of f_z(f_0,z) as a function of source frequency and redsfhit
    zMax = 10.
    zs = np.arange(0.,zMax,0.05)
    dz = zs[1]-zs[0]
    redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])

    # Evolution of mMax allows pretty much arbitrarily large masses at e.g. z=10
    # Cut masses off at 300 Msun, which should merge below LIGO band
    mMin = 7.

    # Set up mass grid
    # Since we'll need rather large masses at high redshift, logarithmically space total masses
    # Compensate by multiplying by Mtots in definition of "probs" below
    # i.e. dM = d(lnM)*M

    nGrid=int(2.*np.log10(mMax_cutoff/mMin)/np.log10(mMax/mMin))+1
    nGrid=max(nGrid,30)
    print(nGrid)
    Mtots = np.logspace(np.log10(2.*mMin),np.log10(2.*mMax_cutoff),nGrid)
    qs = np.linspace(mMin/mMax_cutoff,1,35)
    etas = qs/np.power(1.+qs,2.)

    # Compute energy spectra
    # E(eta,M,z,f)
    energySpectra = np.array([[dEdf(M,redshiftedFreqs,eta=eta) for M in Mtots] for eta in etas])

    # Grid
    Mtots_2d,qs_2d = np.meshgrid(Mtots,qs)
    m1s_2d = Mtots_2d/(1.+qs_2d)
    m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

    # 3D grid to hold p(z,eta,Mtot)
    probs = np.array([1./(1.+qs_2d)*np.power(m1s_2d,-gamma1)*Mtots_2d\
                *np.heaviside(mMax*np.power(1.+z,gamma2)-m1s_2d,0.5)\
                *np.heaviside(mMax*np.power(1.+z,gamma2)-m2s_2d,0.5)\
                *np.heaviside(m1s_2d-mMin,0.5)\
                *np.heaviside(m2s_2d-mMin,0.5)\
                    for z in zs])

    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m1s_2d,0.5))
    #print(m2s_2d[mMax*np.power(1.+zs[0],gamma2)-m1s_2d>=0])
    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m2s_2d,0.5))
    #print(m1s_2d[mMax*np.power(1.+zs[0],gamma2)-m1s_2d>=0])
    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m1s_2d,0.5)*np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m2s_2d,0.5)*np.heaviside(m2s_2d-mMin,0.5))

    # Normalize each 2D subset over redshift
    for i in range(zs.size):
        probs[i,:] /= np.sum(probs[i,:])

    # Compute weighted average of energy-density spectrum
    # For every z_i, contract p(z_i,eta,Mtot)*E(eta,Mtot,z_i,f) to form population averaged dE/df(z,f)
    dedf = np.array([np.tensordot(probs[i,:,:],energySpectra[:,:,i,:],axes=2) for i in range(zs.size)]).T

    # Redshift integrand
    R_invE = R0*dPdz_monotonic(zs,alpha)/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz

    if freqInterp:
        final_Omg_spectrum = np.interp(targetFreqs,freqs,Omg_spectrum,left=0.,right=0.)
    else:
        final_Omg_spectrum = Omg_spectrum

    return final_Omg_spectrum

def OmegaGW_massEvolution_extended(R0,alpha,beta,zpeak,gamma1,gamma2,mMax,targetFreqs,freqInterp=False,mMax_cutoff=300.):

    if freqInterp:
        freqs = np.linspace(10.,500.,100)
    else:
        freqs = targetFreqs

    # Build 2D array of f_z(f_0,z) as a function of source frequency and redsfhit
    zMax = 10.
    zs = np.arange(0.,zMax,0.05)
    dz = zs[1]-zs[0]
    redshiftedFreqs = np.array([freqs*(1.+z) for z in zs])

    # Evolution of mMax allows pretty much arbitrarily large masses at e.g. z=10
    # Cut masses off at 300 Msun, which should merge below LIGO band
    mMin = 7.

    # Set up mass grid
    # Since we'll need rather large masses at high redshift, logarithmically space total masses
    # Compensate by multiplying by Mtots in definition of "probs" below
    # i.e. dM = d(lnM)*M

    nGrid=int(2.*np.log10(mMax_cutoff/mMin)/np.log10(mMax/mMin))+1
    nGrid=max(nGrid,30)
    print(nGrid)
    Mtots = np.logspace(np.log10(2.*mMin),np.log10(2.*mMax_cutoff),nGrid)
    qs = np.linspace(mMin/mMax_cutoff,1,35)
    etas = qs/np.power(1.+qs,2.)

    # Compute energy spectra
    # E(eta,M,z,f)
    energySpectra = np.array([[dEdf(M,redshiftedFreqs,eta=eta) for M in Mtots] for eta in etas])

    # Grid
    Mtots_2d,qs_2d = np.meshgrid(Mtots,qs)
    m1s_2d = Mtots_2d/(1.+qs_2d)
    m2s_2d = qs_2d*Mtots_2d/(1.+qs_2d)

    # 3D grid to hold p(z,eta,Mtot)
    probs = np.array([1./(1.+qs_2d)*np.power(m1s_2d,-gamma1)*Mtots_2d\
                *np.heaviside(mMax*np.power(1.+z,gamma2)-m1s_2d,0.5)\
                *np.heaviside(mMax*np.power(1.+z,gamma2)-m2s_2d,0.5)\
                *np.heaviside(m1s_2d-mMin,0.5)\
                *np.heaviside(m2s_2d-mMin,0.5)\
                    for z in zs])

    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m1s_2d,0.5))
    #print(m2s_2d[mMax*np.power(1.+zs[0],gamma2)-m1s_2d>=0])
    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m2s_2d,0.5))
    #print(m1s_2d[mMax*np.power(1.+zs[0],gamma2)-m1s_2d>=0])
    #print(np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m1s_2d,0.5)*np.heaviside(mMax*np.power(1.+zs[0],gamma2)-m2s_2d,0.5)*np.heaviside(m2s_2d-mMin,0.5))

    # Normalize each 2D subset over redshift
    for i in range(zs.size):
        probs[i,:] /= np.sum(probs[i,:])

    # Compute weighted average of energy-density spectrum
    # For every z_i, contract p(z_i,eta,Mtot)*E(eta,Mtot,z_i,f) to form population averaged dE/df(z,f)
    dedf = np.array([np.tensordot(probs[i,:,:],energySpectra[:,:,i,:],axes=2) for i in range(zs.size)]).T

    # Redshift integrand
    R_invE = R0*dPdz(zs,alpha,beta,zpeak)/np.sqrt(OmgM*(1.+zs)**3.+OmgL)/(1.+zs)

    # Convert to energy density
    Omg_spectrum = (freqs/rhoC/H0)*dedf.dot(R_invE)*dz

    if freqInterp:
        final_Omg_spectrum = np.interp(targetFreqs,freqs,Omg_spectrum,left=0.,right=0.)
    else:
        final_Omg_spectrum = Omg_spectrum

    return final_Omg_spectrum

if __name__=="__main__":

    freqs = np.arange(10.,400.,1)

    gamma1=0.1928562
    gamma2=4.00356808
    mMax=40
    alpha=-11.32862879
    Omgs=OmegaGW_massEvolution(1.,alpha,gamma1,gamma2,mMax,freqs,freqInterp=True)
    print(Omgs[0])
    sys.exit()

    """
    tstart=time.time()
    Omg1 = OmegaGW_massDistr(3.,5.,2.,2,40,freqs)
    tstop=time.time()
    print(tstop-tstart)

    tstart=time.time()
    Omg2 = OmegaGW_massDistr(3.,5.,2.,2,40,freqs,freqInterp=True)
    tstop=time.time()
    print(tstop-tstart)
    """

    R0=1e-9
    alpha=2.
    gamma=2.3
    mMax=40.

    Omg1 = OmegaGW_monotonic(R0,alpha,gamma,mMax,freqs,freqInterp=True)
    #Omg2 = OmegaGW_massEvolution(R0,alpha,gamma,0,mMax,freqs,freqInterp=True)
    Omg_01 = OmegaGW_massEvolution(R0,alpha,gamma,0.1,mMax,freqs,freqInterp=True)
    Omg_03 = OmegaGW_massEvolution(R0,alpha,gamma,0.3,mMax,freqs,freqInterp=True)
    Omg_1 = OmegaGW_massEvolution(R0,alpha,gamma,1,mMax,freqs,freqInterp=True)
    Omg_3 = OmegaGW_massEvolution(R0,alpha,gamma,3,mMax,freqs,freqInterp=True)

    fig,ax = plt.subplots(figsize=(4,3))
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.plot(freqs,Omg1,ls='--',label='Reference')
    #ax.plot(freqs,Omg2)
    ax.plot(freqs,Omg_01,label=r'$\gamma_2=0.1$')
    ax.plot(freqs,Omg_03,label=r'$\gamma_2=0.3$')
    ax.plot(freqs,Omg_1,label=r'$\gamma_2=1$')
    ax.plot(freqs,Omg_3,label=r'$\gamma_2=3$')
    ax.set_xlabel('f (Hz)')
    ax.set_ylabel('SGWB')
    plt.legend(loc='lower right')
    plt.tight_layout()
    plt.savefig('OmgComparison.pdf',bbox_inches='tight')


    """
    #freqs,dEdf_spectrum = dEdf(20.)
    fig,ax = plt.subplots(figsize=(4,3))
    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.plot(freqs,Omg1)
    ax.plot(freqs,Omg2,ls='--')
    plt.savefig('test2.pdf')
    """

    """
    mMin = 5.
    mMax = 40.
    a = 2.
    nTrials = 1000
    cdfs = np.random.random(nTrials)
    m1s = mMin*np.power(1.-cdfs*(1.-np.power(mMax/mMin,1.-a)),1./(1.-a))
    cdfs = np.random.random(nTrials)
    m2s = mMin + cdfs*(m1s-mMin)

    etas = (m1s*m2s)/np.power(m1s+m2s,2.)
    Mtots = m1s+m2s
    fig,ax = plt.subplots()
    ax.hist(Mtots,bins=15)
    plt.savefig('Mtot.pdf')
    """

